#ifndef __MYCGPATH__
#define __MYCGPATH__
#include "Fly.h"
#include "MyCG_util.h"
#include "MyCG_limit.h"
#include "MyCG.h"
#include "MyCG_stage.h"
#include <list>




//const int dir = 4; // number of possible directions to go at any position
//static int grid_dx[dir] = { 1, 0, -1,  0};
//static int grid_dy[dir] = { 0, 1,  0, -1};
//static int terrain_dx[dir] = { 1, 0, -1, 0 };
//static int terrain_dy[dir] = { 0, -1, 0, 1 };


const int dir = 8; // number of possible directions to go at any position
static int grid_dx[dir] = { 1, 1, 0, -1, -1, -1, 0, 1 };
static int grid_dy[dir] = { 0, 1, 1, 1, 0, -1, -1, -1 };
static int terrain_dx[dir] = { 1, 1, 0, -1, -1, -1, 0, 1 };
static int terrain_dy[dir] = { 0, -1, -1, -1, 0, 1, 1, 1 };

float inner_nodes_pose[MAX_INNER_NODES][3];

//float now_imagine_pos[3];
//int now_imagine_grid[2];
/*
Axis.
X ---->
Y
|
|
V

Direction
5  6  7
\ | /
4<-      -> 0
/ | \
3  2  1


Map indicator
0 == Walkable
1 == Obstacle
*/

class node
{
	// current position
	int xPos;
	int yPos;
	// total distance already travelled to reach the node
	int level;
	float cost;
	// priority=level+remaining distance estimate
	float priority;  // smaller: higher priority

public:
	node(int xp, int yp, int d, int p, float _gridmap_cost[][MAX_GRID_Y])
	{
		xPos = xp; yPos = yp; level = d; priority = p;

		//weighted level;
		cost =  (_gridmap_cost[xPos][yPos]);
	}

	int getxPos() const { return xPos; }
	int getyPos() const { return yPos; }
	int getLevel() const { return level; }
	int getPriority() const { return priority; }

	void updatePriority(const int & xDest, const int & yDest)
	{
		priority = (level + estimate(xDest, yDest) * 1) + cost; //A*
		//priority = (level + estimate(xDest, yDest) * 10) ; //A*
	}

	// give better priority to going strait instead of diagonally
	void nextLevel(const int & i) // i: direction
	{
		level += (dir == 8 ? (i % 2 == 0 ? 10 : 14) : 10);
	}

	// Estimation function for the remaining distance to the goal.
	const int & estimate(const int & xDest, const int & yDest) const
	{
		static int xd, yd, d;
		xd = xDest - xPos;
		yd = yDest - yPos;

		// Euclidian Distance
		d = static_cast<int>(sqrt(xd*xd + yd*yd));

		// Manhattan distance
		//d=abs(xd)+abs(yd);

		// Chebyshev distance
		//d=max(abs(xd), abs(yd));

		return(d);
	}
};

// Determine priority (in the priority queue)
bool operator<(const node & a, const node & b)
{
	return a.getPriority() > b.getPriority();
}
double Area(double dX0, double dY0, double dX1, double dY1, double dX2, double dY2)
{
	double dArea = ((dX1 - dX0)*(dY2 - dY0) - (dX2 - dX0)*(dY1 - dY0)) / 2.0;
	return (dArea > 0.0) ? dArea : -dArea;
}
bool isWithoutObstacle(const int & _xStart, const int & _yStart,
	const int & _xFinish, const int & _yFinish, int map[][MAX_GRID_Y]){


	// triangle area> (p1+p2)*1/2 => pass
	bool withoutObstacles = true;
	int x, y;
	int yStart  = min(_yStart, _yFinish);
	int yFinish = max(_yStart, _yFinish);
	int xStart = min(_xStart, _xFinish);
	int xFinish = max(_xStart, _xFinish);
	for (y = yStart; y<=yFinish; y++){
		for (x = xStart; x<=xFinish; x++){
			if (Area(xStart, yStart, xFinish, yFinish, x, y) <
				sqrt((xStart - xFinish)*(xStart - xFinish) + (yStart - yFinish)*(yStart - yFinish)*2)
				){
				if (map[x][y] == 1){
					withoutObstacles = false;
				}
			}
		}
	}




	/*
	// if there is no obstacle between them, go straight
	int x, y;
	bool withoutObstacles = true;
	// only check the x1,y1 -> x2,y2 box noodes.
	for (y = yStart; y<yFinish; y++){
		for (x = xStart; x<xFinish; x++){
			if (map[x][y] == 1){
				withoutObstacles = false;
			}
		}
	}
	*/
	return withoutObstacles;
}
std::string pathFind(const int & xStart, const int & yStart,
	const int & xFinish, const int & yFinish, int map[][MAX_GRID_Y], float gridmap_cost[][MAX_GRID_Y]){
	static std::priority_queue<node> pq[2]; // list of open (not-yet-tried) nodes
	static int pqi; // pq index
	static node* n0;
	static node* m0;
	static int i, j, x, y, xdx, ydy;
	static char c;
	pqi = 0;

	int closed_nodes_map[MAX_GRID_X][MAX_GRID_Y]; // map of closed (tried-out) nodes
	int open_nodes_map[MAX_GRID_X][MAX_GRID_Y]; // map of open (not-yet-tried) nodes
	int dir_map[MAX_GRID_X][MAX_GRID_Y]; // map of directions

	// 
	if (isWithoutObstacle(xStart, yStart, xFinish, yFinish, map)){
		return ""; // no route found
	}


	// reset the node maps
	for (y = 0; y<MAX_GRID_Y; y++){
		for (x = 0; x<MAX_GRID_X; x++){
			closed_nodes_map[x][y] = 0;
			open_nodes_map[x][y] = 0;
		}
	}

	// create the start node and push into list of open nodes
	n0 = new node(xStart, yStart, 0, 0, gridmap_cost);
	n0->updatePriority(xFinish, yFinish);
	pq[pqi].push(*n0);
	open_nodes_map[x][y] = n0->getPriority(); // mark it on the open nodes map

	// A* search
	while (!pq[pqi].empty())
	{
		// get the current node w/ the highest priority
		// from the list of open nodes
		n0 = new node(pq[pqi].top().getxPos(), pq[pqi].top().getyPos(),
			pq[pqi].top().getLevel(), pq[pqi].top().getPriority(), gridmap_cost);

		x = n0->getxPos(); y = n0->getyPos();

		pq[pqi].pop(); // remove the node from the open list
		open_nodes_map[x][y] = 0;
		// mark it on the closed nodes map
		closed_nodes_map[x][y] = 1;

		// quit searching when the goal state is reached
		//if((*n0).estimate(xFinish, yFinish) == 0)
		if (x == xFinish && y == yFinish)
		{
			// generate the path from finish to start
			// by following the directions
			std::string path = "";
			while (!(x == xStart && y == yStart))
			{
				j = dir_map[x][y];
				c = '0' + (j + dir / 2) % dir;
				path = c + path;
				x += grid_dx[j];
				y += grid_dy[j];
			}

			// garbage collection
			delete n0;
			// empty the leftover nodes
			while (!pq[pqi].empty()) pq[pqi].pop();
			return path;
		}

		// generate moves (child nodes) in all possible directions
		for (i = 0; i<dir; i++)
		{
			xdx = x + grid_dx[i]; ydy = y + grid_dy[i];

			if (!(xdx<0 || xdx>MAX_GRID_X - 1 || ydy<0 || ydy>MAX_GRID_Y - 1 || map[xdx][ydy] == 1
				|| closed_nodes_map[xdx][ydy] == 1))
			{
				// generate a child node
				m0 = new node(xdx, ydy, n0->getLevel(),
					n0->getPriority(), gridmap_cost);
				m0->nextLevel(i);
				m0->updatePriority(xFinish, yFinish);

				// if it is not in the open list then add into that
				if (open_nodes_map[xdx][ydy] == 0)
				{
					open_nodes_map[xdx][ydy] = m0->getPriority();
					pq[pqi].push(*m0);
					// mark its parent node direction
					dir_map[xdx][ydy] = (i + dir / 2) % dir;
				}
				else if (open_nodes_map[xdx][ydy]>m0->getPriority())
				{
					// update the priority info
					open_nodes_map[xdx][ydy] = m0->getPriority();
					// update the parent direction info
					dir_map[xdx][ydy] = (i + dir / 2) % dir;

					// replace the node
					// by emptying one pq to the other one
					// except the node to be replaced will be ignored
					// and the new node will be pushed in instead
					while (!(pq[pqi].top().getxPos() == xdx &&
						pq[pqi].top().getyPos() == ydy))
					{
						pq[1 - pqi].push(pq[pqi].top());
						pq[pqi].pop();
					}
					pq[pqi].pop(); // remove the wanted node

					// empty the larger size pq to the smaller one
					if (pq[pqi].size()>pq[1 - pqi].size()) pqi = 1 - pqi;
					while (!pq[pqi].empty())
					{
						pq[1 - pqi].push(pq[pqi].top());
						pq[pqi].pop();
					}
					pqi = 1 - pqi;
					pq[pqi].push(*m0); // add the better node instead
				}
				else delete m0; // garbage collection
			}
		}
		delete n0; // garbage collection
	}
	return ""; // no route found
}
bool findpath(float* src, float* dst, std::list<float*> &ret){
	//convert to grid
	float x_offset = (((MAX_DETECT_X - MIN_DETECT_X) / MAX_GRID_X) + 1);
	float y_offset = (((MAX_DETECT_Y - MIN_DETECT_Y) / MAX_GRID_Y) + 1);

	int src_grid[2];
	coor_to_grid(src[0], src[1], src_grid[0], src_grid[1]);

	int dst_grid[2];
	coor_to_grid(dst[0], dst[1], dst_grid[0], dst_grid[1]);

	//int map[][]=extern int current_gridmap_status[][];
	extern int current_gridmap_status[MAX_GRID_X][MAX_GRID_Y];
	extern float gridmap_cost[MAX_GRID_X][MAX_GRID_Y];
	std::string path = pathFind(src_grid[0], src_grid[1], dst_grid[0], dst_grid[1], current_gridmap_status, gridmap_cost);

	const char *c_path = path.c_str();

	//Debug Text
	/*
	char pathS[256];
	sprintf(pathS, "calculated path from %d,%d -> %d,%d => %s \n", src_grid[0], src_grid[1], dst_grid[0], dst_grid[1], c_path);
	OutputDebugString(pathS);
	sprintf(pathS, "calculated path from grid %.1f,%.1f,%.1f -> %.1f,%.1f,%.1f => %s \n", src[0], src[1], src[2], dst[0], dst[1], dst[2], c_path);
	OutputDebugString(pathS);
	*/
	/*
	Direction

	5  6  7
	\ | /
	4<-      -> 0
	/ | \
	3  2  1
	*/

	float* now_imagine_pos;
	float  last_imagine_pos[3];

	last_imagine_pos[0] = src[0];
	last_imagine_pos[1] = src[1];
	last_imagine_pos[2] = src[2];
	char last_direction = 'n';
	int now_nodes = 0;
	for (char& c : path) {

		if (last_direction == c){
			ret.pop_back();
		}
		last_direction = c;

		int i = c - 48; // i is now equal to 1, not '1'

		if (now_nodes == 0){
			inner_nodes_pose[now_nodes][0] = src[0] + x_offset*terrain_dx[i];
			inner_nodes_pose[now_nodes][1] = src[1] + y_offset*terrain_dy[i];
			inner_nodes_pose[now_nodes][2] = src[2];
		}
		if (now_nodes >= 1){
			inner_nodes_pose[now_nodes][0] = inner_nodes_pose[now_nodes - 1][0];
			inner_nodes_pose[now_nodes][1] = inner_nodes_pose[now_nodes - 1][1];
			inner_nodes_pose[now_nodes][2] = inner_nodes_pose[now_nodes - 1][2];

			inner_nodes_pose[now_nodes][0] += x_offset*terrain_dx[i];
			inner_nodes_pose[now_nodes][1] += y_offset*terrain_dy[i];
			inner_nodes_pose[now_nodes][2] = src[2];
		}

		ret.push_back(inner_nodes_pose[now_nodes]);
		now_nodes++;

	}
	if (now_nodes > 0){
		ret.pop_back();
	}

	ret.push_back(dst);
	return true;
}
bool findpath(char_info& src, float* dst, JobType jt, char_info* target=NULL, skill* jobskill=NULL){
	std::list<float*> tmp;
	float aPos[3];
	src.actor.GetPosition(aPos);
	findpath(aPos, dst, tmp);

	src.active = std::queue<CG_job>();

	auto it = tmp.begin();
	for (; it != tmp.end(); it++){
		src.active.push(CG_job(*it, (void*)target, 0, jt, jobskill));
	}
	it--;
	src.active.push(CG_job(*it, (void*)target, -1, jt, jobskill));
	return true;
}
#endif
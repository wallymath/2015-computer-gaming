#ifndef __MYCG__
#define __MYCG__
#define __CGDEBUG__
#include "Fly.h"
#include "MyCG_util.h"
#include "MyCG_limit.h"
#include "skill.h"
#include "json.h"
#include <vector>
#include <queue>
#include <list>
#include <map>
#include <set>


struct char_attr{
	rf _speed,_AD, _AP;
	CHARACTERid actorID;
	float* mouseThen, *posThen;
	char_attr(rf& speed, rf& AD, rf& AP, CHARACTERid aid) : _speed(speed), _AD(AD), _AP(AP), actorID(aid){};
};

struct char_status{
	float _max_blood;
	rf cur_blood;
	float _max_MP;
	rf cur_MP;
	char_attr _attr;
	char_status(float max_blood = 50.0, float max_MP = 50.0, float speed = 6.0, float AD = 15.0, float AP = 0.0, CHARACTERid aid = 0) : cur_blood(max_blood, 0.0f, max_blood), _max_blood(max_blood), cur_MP(max_MP, 0.0f, max_MP), _max_MP(max_MP), _attr(rf(MAX_SPEED, 0.0, speed), rf(MAX_AD, 0.0, AD), rf(MAX_AP, 0.0, AP), aid){}
};

struct CG_event{
	float insert;		//insert time
	float delay;		// delay time
	float last;			// last period
	char_attr atk_attr;	//attacker's attribute
	skill_params atk_params;    
	fptr callback;   
	CG_event(float insertTime, float delayTime, float lastTime, char_attr atkAttr, skill_params sp, fptr callBack) :insert(insertTime), delay(delayTime), last(lastTime), atk_params(sp), atk_attr(atkAttr), callback(callBack){}
};

struct CG_job{
	float* dst;	// target destination
	void* target;	// target character in attack mode
	float last;	// time to stay after arrive dst, -1 for guard dst position
	skill* job_sk;
	float done; // Filled when job is done
	JobType type;
	CG_job(float* destination, void* targetCharacter = NULL, float lastTime = 0, JobType jobtype = JobType::move, skill* skf = NULL) :dst(destination), target(targetCharacter), last(lastTime), type(jobtype), job_sk(skf){ done = -1; }
};

struct Effect{
	GAMEFX_SYSTEMid FXID;
	bool once;
	bool operator ==(Effect c){ return FXID == c.FXID; }
	Effect(GAMEFX_SYSTEMid fid, bool o) : FXID(fid), once(o){};
};

class char_info {
public:
	CHARACTERid actorID;
	ACTIONid idleID, runID, guardID, curPoseID, updatePoseID, atk1ID, atk2ID, dam1ID, dam2ID, dieID;
	ACTIONid skID[SKILL_NUM];
	skill* _skillSet;
	OBJECTid bloodBarObj;
	GEOMETRYid bloodBar;
	SCENEid sID;
	FnCharacter actor;
	FnObject* model;

	std::set<SPEffect> flag;
	std::vector<Effect> fxs;
	std::map<CHARACTERid, float> hate;
	CHARACTERid maxHate;
	char* char_name;
	bool combating, canDamage;
	std::list<CG_event> event_queue;
	std::queue<CG_job> active;
	std::vector<CG_job> routine;
	int job_ptr;	// routine = const circular queue = vector+job_ptr
	int act_job;	// 0 = active, 1 = routine;
	float* size;
	float* color;
	char_status* stat;
	fptr AI;	//AI function
	CampType camp;	//0 = enemy, 1=player1, 2=player2
	void init_ID(CHARACTERid _aID, SCENEid _sID, char* _char_name, CampType _camp = enemy);
	void init_bloodBar(float _size[2], float _color[3], char_status* _status);
	void init_terrain(ROOMid tID, float hlimit);
	bool init_pos(float pos[3], float fDir[3], float uDir[3]);
	void init_action(char** actions, int length);
	void init_skill(Json::Value root, char* name);
	void init_routine(std::vector<CG_job> job);
	void init_AI(AIType t);
	void back_routine();
	inline bool update_pos(){
		if (curPoseID != updatePoseID && curPoseID != dieID){
			curPoseID = updatePoseID;
			actor.SetCurrentAction(NULL, 0, curPoseID);
			return true;
		}
		return false;
	}
	
	int adjust_blood(float amount);
	bool operator ==(char_info c){ return actorID == c.actorID; }
	skill* loadSkill(Json::Value root, char* name);
};

bool checkEnemy(CampType a, CampType b);
bool isLoop(char_info* c, ACTIONid aid);
#endif
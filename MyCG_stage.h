#ifndef __MYCGSTAGE__
#define __MYCGSTAGE__
#include "Fly.h"
#include "MyCG_util.h"
#include "MyCG_limit.h"
#include "MyCG.h"
#include "json.h"
#include <stdio.h>
#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>
Json::Value parseSkillJson(char* name);
bool coor_to_grid(float terrain_x, float terrain_y, int& grid_x, int& grid_y){
	float x_offset = (((MAX_DETECT_X - MIN_DETECT_X) / MAX_GRID_X) + 1);
	float y_offset = (((MAX_DETECT_Y - MIN_DETECT_Y) / MAX_GRID_Y) + 1);


	grid_x = ((terrain_x - MIN_DETECT_X) / (MAX_DETECT_X - MIN_DETECT_X))*MAX_GRID_X;
	//grid_x = MAX_GRID_X - ((terrain_x - MIN_DETECT_X) / (MAX_DETECT_X - MIN_DETECT_X))*MAX_GRID_X;
	grid_y = MAX_GRID_Y - ((terrain_y - MIN_DETECT_Y) / (MAX_DETECT_Y - MIN_DETECT_Y))*MAX_GRID_Y;

	return true;
}

bool grid_to_coor(float& terrain_x, float& terrain_y, int grid_x, int grid_y){
	float x_offset = (((MAX_DETECT_X - MIN_DETECT_X) / MAX_GRID_X) + 1);
	float y_offset = (((MAX_DETECT_Y - MIN_DETECT_Y) / MAX_GRID_Y) + 1);

	terrain_x = MIN_DETECT_X + x_offset*grid_x;
	//terrain_x = MAX_DETECT_X - x_offset*grid_x;
	terrain_y = MAX_DETECT_Y - y_offset*grid_y;
	return true;
}
bool load_stage(int stage_num, ROOMid trID, OBJECTid tID, SCENEid sID, std::vector<char_info>& all_char, int map[][MAX_GRID_Y], float gridmap_cost[][MAX_GRID_Y]){
	FnScene scene;
	scene.ID(sID);

	char buf[100000], line[1000];
	buf[0] = '\0';
	FILE* fp = fopen("skill.json", "r");
	while (fgets(line, 1000, fp) != NULL) {
		strcat(buf, line);
	}
	fclose(fp);
	Json::Value root   = parseSkillJson(buf);
	Json::Value skills = root["skills"];
	Json::Value chars  = root["chars"];


	if (stage_num == 0){
		for (int i = 0; i < chars.size(); i++) {
			const Json::Value info = chars[i];

			char_info pawn;
			std::vector<CG_job> job;

			CampType camp = CampType(atoi(info["camp"].asString().c_str()));
			char* fname=(char*)malloc(sizeof(char)*20); 
			fname[0] = '\0'; strcpy(fname, info["fname"].asString().c_str());
			char* name=(char*)malloc(sizeof(char)*20);  
			name[0] = '\0';  strcpy(name, info["name"].asString().c_str());
			float blood = atof(info["blood"].asString().c_str());
			float mana = atof(info["mana"].asString().c_str());
			float speed = atof(info["speed"].asString().c_str());
			float ad = atof(info["ad"].asString().c_str());
			float ap = atof(info["ap"].asString().c_str());

			float* pos = new float[3];
			for (int i = 0; i < 3; i++) {
				pos[i] = atof(info["pos"][i].asString().c_str());
			}

			float* facing = new float[3];
			for (int i = 0; i < 3; i++)
				facing[i] = atof(info["facing"][i].asString().c_str());

			int numAction = info["actions"].size();
			char** actions = new char*[numAction];
			for (int i = 0; i < numAction; i++) {
				actions[i] = new char[20];
				strcpy(actions[i], info["actions"][i].asString().c_str());
			}

			for (int j = 0; j < info["job"].size(); j++) {
				float* jobPos = new float[3];
				for (int i = 0; i < 3; i++)
					jobPos[i] = atof(info["job"][j][i].asString().c_str());

				job.push_back(CG_job(jobPos, NULL, atoi(info["job"][j][3].asString().c_str()), JobType::move));
			}
			pawn.init_routine(job);

			pawn.init_ID(scene.LoadCharacter(fname), sID, name, camp);
			pawn.init_bloodBar(new float[2]{20.0f, 2.0f}, new float[3]{ 1.0f, 0.0f, 0.0f }, new char_status(blood, mana, speed, ad, ap, pawn.actorID));
			pawn.init_terrain(trID, 10.0f);
			pawn.init_pos(pos, facing, new float[3]{ 0.0f, 0.0f, 1.0f });
			pawn.init_action(actions, numAction);
			pawn.init_skill(skills, name);
			if (pawn.camp == enemy)
				pawn.init_AI(normal);
			all_char.push_back(pawn);
			job.clear();
		}
	}

	//construct a gridmap
	// setting the gridmap_status, should  be replaced with a collision box brute force detection.
	int y, x,i,j;
	for (y = 0; y<MAX_GRID_Y; y++){
		for (x = 0; x<MAX_GRID_X; x++){
			map[x][y] = 0;
		}
	}

	FnObject terrain;
	terrain.ID(tID);
	float  n[3] = { 0, 0, -1.0f };

	float start_detect_z = 1000;
	float global_x;
	float global_y;
	float x_offset = ((MAX_DETECT_X - MIN_DETECT_X) / MAX_GRID_X) + 1;
	float y_offset = ((MAX_DETECT_Y - MIN_DETECT_Y) / MAX_GRID_Y) + 1;

	for (y = 0; y < MAX_GRID_Y; y++){
		for (x = 0; x < MAX_GRID_X; x++){
			grid_to_coor(global_x, global_y, x, y);
			float cpos[3] = { global_x, global_y, start_detect_z };
			if (terrain.HitTest(cpos, n) >= 0){	// detection object  on terrain
				map[x][y] = 0;

			}
			else{
				map[x][y] = 1;
			}

		}
	}

	int c = COST_RADIUS;
	for (y = c; y < MAX_GRID_Y-c; y++){
		for (x = c; x < MAX_GRID_X-c; x++){
			if (map[x][y] == 1){
				for (j = y-c; j <= y+c; j++){
					for (i = x-c; i <= x+c; i++ ){
						gridmap_cost[i][j] += 7*(1.0/c);
					}
				}
			}
		}
	}
	
	std::ofstream myfile;
	char filename[256];
	sprintf(filename, "cost_matrix_%d.txt", stage_num);
	myfile.open(filename);
	for (y = 0; y < MAX_GRID_Y; y++){
		for (x = 0; x < MAX_GRID_X; x++){
			char costS[256];
			sprintf(costS, "%.1f ", gridmap_cost[x][y]);
			myfile << costS;
		}
		myfile << "\n";
	}
	myfile.close();
	


	return true;
}

Json::Value parseSkillJson(char* name) {
	Json::Value root;   // will contains the root value after parsing.
	Json::Reader reader;
	bool parsingSuccessful = reader.parse(name, root);
	if (!parsingSuccessful) {
		// report to the user the failure and their locations in the document.
		char msg[256];
		sprintf(msg, "Failed to parse configuration %s\n", reader.getFormattedErrorMessages().c_str());

		return NULL;
	}
	return root;
}
#endif
#include "Fly.h"
#include "MyCG_util.h"
#include "MyCG.h"
#include "json/json.h"
#include "skill.h"
#include "MyCG_AI.h"
#include <string.h>
#include <vector>
using namespace std;
int tempfn(void* a, void* b){
	char_attr *atk = (char_attr *)a;
	char_info *me = (char_info*)b;
	//me->adjust_blood(-40.0);
	return 0;
}
void char_info::init_ID(CHARACTERid _aID, SCENEid _sID, char* _char_name, CampType _camp){
	actorID = _aID;
	actor.ID(_aID);
	sID = _sID;
	camp = _camp;
	char_name = _char_name;
	maxHate = -1;
}
void char_info::init_bloodBar(float _size[2], float _color[3], char_status* _status){
	bloodBarObj = FnScene(sID).CreateObject(MODEL);
	model = new FnObject(bloodBarObj);
	size = _size;
	color = _color;
	stat = _status;
	bloodBar = (*model).Billboard(NULL, size, NULL, 0, color);
	(*model).SetParent(actor.GetBaseObject());
	(*model).Translate(0.0f, 0.0f, 90.0f, REPLACE);
	canDamage = true;
}
void char_info::init_terrain(ROOMid tID, float hlimit){
	actor.SetTerrainRoom(tID, hlimit);
}
bool char_info::init_pos(float pos[3], float fDir[3], float uDir[3]){
	actor.SetDirection(fDir, uDir);
	// make the blood bar's parent is the base and raise up the blood bar higher than the head of lyubu
	return actor.PutOnTerrain(pos);
}
void char_info::init_action(char** actions, int length){
	for (int i = 0; i < length; i++){
		switch (i){
		case 0:
			idleID = actor.GetBodyAction(NULL, actions[i]);
			break;
		case 1:
			runID = actor.GetBodyAction(NULL, actions[i]);
			break;
		case 2:
			guardID = actor.GetBodyAction(NULL, actions[i]);
			break;
		case 3:
			atk1ID = actor.GetBodyAction(NULL, actions[i]);
			break;
		case 4:
			dam1ID = actor.GetBodyAction(NULL, actions[i]);
			break;
		case 5:
			dieID = actor.GetBodyAction(NULL, actions[i]);
			break;
		}
	}
	curPoseID = runID;
	actor.SetCurrentAction(NULL, 0, curPoseID);
	actor.Play(START, 0.0f, FALSE, TRUE);
	combating = false;
}

void char_info::init_skill(Json::Value root, char* name){
	_skillSet = loadSkill(root, name);
}

void char_info::init_routine(vector<CG_job> job){
	for (std::vector<CG_job>::iterator it = job.begin(); it != job.end(); it++){
		routine.push_back(*it);
	}
	act_job = 1;
	job_ptr = 0;
}

void char_info::init_AI(AIType t){
	switch (t){
	case normal:
		AI = normalAI;
		break;
	}
}

skill* char_info::loadSkill(Json::Value root, char* name){
	const Json::Value skills = root[name];

	skill* ret = (skill*)malloc(sizeof(skill)*4);
	for (int i = 0; i < 4; i++) {
		ret[i].sk = NULL;
	}
	for (int i = 0; i < skills.size(); i++) {
		
		SkillType stype = SkillType(atoi(skills[i]["Stype"].asString().c_str()));
		DirectionType dtype = DirectionType(atoi(skills[i]["Dtype"].asString().c_str()));
		SPEffect sp_effect = SPEffect(atoi(skills[i]["sp_effect"].asString().c_str()));
		float base = atof(skills[i]["base"].asString().c_str());
		float range = atof(skills[i]["range"].asString().c_str());
		float width = atof(skills[i]["width"].asString().c_str());
		float angle = atof(skills[i]["angle"].asString().c_str());
		float skill_delay = atof(skills[i]["sdelay"].asString().c_str());
		float event_delay = atof(skills[i]["edelay"].asString().c_str());
		float ad_b = atof(skills[i]["ad_buf"].asString().c_str());
		float ap_b = atof(skills[i]["ap_buf"].asString().c_str());
		float last = atof(skills[i]["last"].asString().c_str());
		float cd = atof(skills[i]["cd"].asString().c_str());
		float dot = atof(skills[i]["dot"].asString().c_str());
		float dot_interval = atof(skills[i]["dot_interval"].asString().c_str());
		float sp_value = atof(skills[i]["sp_value"].asString().c_str());
		int	  dot_times = atoi(skills[i]["dot_times"].asString().c_str());

		char sfx_name[20], efx_name[20], action_name[20];

		sprintf(action_name, skills[i]["action"].asString().c_str());
		sprintf(sfx_name, skills[i]["skill_fx"].asString().c_str());
		sprintf(efx_name, skills[i]["effect_fx"].asString().c_str());
		skID[i] = actor.GetBodyAction(NULL, action_name);
		skill_params sp(stype, dtype, base, range, angle, width, skill_delay, event_delay, last, sfx_name, action_name, ad_b, ap_b, cd, dot, dot_times, dot_interval, sp_value, -1, sp_effect);
		skill sk(&directional_skill, sp);
		ret[i] = sk;
	}
	return ret;
}



int char_info::adjust_blood(float amount){
	(*stat).cur_blood += amount;
	*size = 20.0f*(*stat).cur_blood.ratio();
	FnBillboard bb(bloodBar);
	bb.SetPositionSize(NULL, size);
	if ((*stat).cur_blood == 0) {	//character died, remove all event queue and all job, 
		act_job = 1;
		active = std::queue<CG_job>();
		canDamage = false; 
		updatePoseID = dieID; 
		update_pos();
	}
	return (*stat).cur_blood > 0;
}
bool checkEnemy(CampType a, CampType b) {
	return (a == enemy) ^ (b == enemy);
}

bool isLoop(char_info* c, ACTIONid aid){
	if (aid == c->runID || aid == c->idleID || aid == c->guardID)	return true;
	return false;
}
#ifndef __MYCGLIMIT__
#define __MYCGLIMIT__


#define SKILL_NUM 4
#define MAX_SPEED 1000.0
#define MAX_AD 10000.0
#define MAX_AP 10000.0
#define MIN_DIS 0.1
#define MAX_GRID_X 100
#define MAX_GRID_Y 100
#define MAX_INNER_NODES 800
#define MAX_DETECT_X 4000
#define MAX_DETECT_Y 4000
#define MIN_DETECT_X -4000
#define MIN_DETECT_Y -4000
#define COST_RADIUS 7
#define UIPath "MyCG_art\\"

#endif
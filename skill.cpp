#include "skill.h"
#include "MyCG.h"
#include "MyCG_util.h"
#include "event.h"
#define PI 3.14159265

float directional_skill(void*src, void*dst, void* sp){
	char_info	*_src = (char_info*)src;
	char_info	*_dst = (char_info*)dst;
	skill_params*_sp = (skill_params*)sp;

	if (_sp->last_cast + _sp->CD > FyTimerCheckTime(0)) {
		// Output "skill is cooling down..."
		return 0.f;
	}
	else{
		_sp->last_cast = FyTimerCheckTime(0);
	}

	float srcPos[3];
	_src->actor.GetPosition(srcPos);

	// Get mouse position
	//extern float mouseX, mouseY, mouseZ;
	float mouseX = _src->stat->_attr.mouseThen[0];
	float mouseY = _src->stat->_attr.mouseThen[1];
	float mouseZ = _src->stat->_attr.mouseThen[2];
	extern std::vector<char_info> all_char;
	
	float fxPos[3];
	if (_sp->Dtype == circle) {
		fxPos[0] = mouseX; 
		fxPos[1] = mouseY; 
		fxPos[2] = mouseZ;
	}
	else {
		fxPos[0] = srcPos[0];
		fxPos[1] = srcPos[1];
		fxPos[2] = srcPos[2];
	}
	

	playFX(fxPos, _sp->last == -1, _sp->fx_name, _sp->event_delay + _sp->last, _src, _sp);

	// Turn character 
	_src->actor.SetDirection(new float[3]{mouseX - srcPos[0], mouseY - srcPos[1], 0}, new float[3]{0.f, 0.f, 1.f});

	// If skill has delay => stuff a buf into src event_queue
	if (_sp->skill_delay > 0) {
		float tmp = _sp->skill_delay;

		_sp->skill_delay = 0;
		_src->stat->_attr.mouseThen	= new float[3]{mouseX, mouseY, mouseZ};
		_src->stat->_attr.posThen	= new float[3]{srcPos[0], srcPos[1], srcPos[2]};

		_src->event_queue.push_back(CG_event(FyTimerCheckTime(0), tmp, _sp->last, _src->stat->_attr, *_sp, &delay_skill));
		_sp->skill_delay = tmp;
		return 1;
	}
	
	char debug[256];
	sprintf(debug, "In Skill: %f %f %f\n", mouseX, mouseY, mouseZ);
	//OutputDebugString(debug);

	bool success = false;
	if (executeDirectionalSkill(_src, _dst, _sp, new float[3]{mouseX, mouseY, mouseZ}, srcPos, fxPos)) {

	}
	return 1;
}

GAMEFX_SYSTEMid playFX(float fxPos[], bool once, char* name, float last, void* src, void* sp) {
	char_info	*_src = (char_info*)src;
	skill_params*_sp = (skill_params*)sp;


	extern SCENEid sID;
	extern std::vector<Effect> FXs;

	FnScene scene(sID);

	GAMEFX_SYSTEMid fid = scene.CreateGameFXSystem();
	FnGameFXSystem gxS(fid);

	BOOL4 beOK = gxS.Load(name, TRUE);
	if (beOK) {
		gxS.SetPlayLocation(fxPos);
	}

	char time[256];
	if (once) {
		FXs.push_back(Effect(fid, true));
	}
	else {
		OutputDebugString("inside play loop\n");

		FXs.push_back(Effect(fid, false));
		_sp->fxID = fid;
		_src->event_queue.push_back(CG_event(FyTimerCheckTime(0), last, -1.f, _src->stat->_attr, *_sp, &remove_fx));
	}
	return fid;
}

bool executeDirectionalSkill(void*src, void*dst, void* sp, float mousePos[], float srcPos[], float* fxPos) {
	char_info	*_src = (char_info*)src;
	char_info	*_dst = (char_info*)dst;
	skill_params*_sp = (skill_params*)sp;

	extern std::vector<char_info> all_char;
	float mouseX = mousePos[0], mouseY = mousePos[1], mouseZ = mousePos[2];
	float dir[2] = { mouseX - srcPos[0], mouseY - srcPos[1] };
	bool success = false;

	// Get all characters
	for (auto it = all_char.begin(); it != all_char.end(); ++it) {
		success = false;
		if (it->actorID == _src->actorID)
			continue;
		if (!checkEnemy(it->camp, _src->camp) && _sp->Stype == damage)
			continue;
		if (checkEnemy(it->camp, _src->camp) && _sp->Stype == heal)
			continue;

		float dstPos[3];
		it->actor.GetPosition(dstPos);

		float origin[3];
		switch (_sp->Dtype) {
		case circle:
			origin[0] = mouseX; origin[1] = mouseY, origin[2] = mouseZ;
			//char msg[256];
			//OutputDebugString("inside circle\n");
			if (dis2D(origin, srcPos) > _sp->range)
				break;
			fxPos[0] = origin[0], fxPos[1] = origin[1], fxPos[2] = origin[2]; 

			if (insideCircle(origin, _sp->width, dstPos)) {
				//sprintf(msg, "person %d got buff\n", _dst->actorID);
				//OutputDebugString(msg);
				success = true;
				it->event_queue.push_back(CG_event(FyTimerCheckTime(0), _sp->event_delay, _sp->last, _src->stat->_attr, *_sp, &point_atk_event));
			}
			break;
		case line:
			fxPos[0] = srcPos[0], fxPos[1] = srcPos[1], fxPos[2] = srcPos[2];
			if (insideLine(srcPos, dstPos, _sp->width, _sp->range, dir)) {
				//OutputDebugString("Pass test\n");

				success = true;
				it->event_queue.push_back(CG_event(FyTimerCheckTime(0), _sp->event_delay, _sp->last, _src->stat->_attr, *_sp, &point_atk_event));
			}
			break;
		case cone:
			OutputDebugString("inside cone\n");
			fxPos[0] = origin[0], fxPos[1] = origin[1], fxPos[2] = origin[2];
			//OutputDebugString("Inside cone\n");
			if (insideCone(srcPos, dstPos, _sp->angle, _sp->range, dir)) {
				//OutputDebugString("Pass cone test\n");

				OutputDebugString("inside cone and hit\n");
				success = true;
				it->event_queue.push_back(CG_event(FyTimerCheckTime(0), _sp->event_delay, _sp->last, _src->stat->_attr, *_sp, &point_atk_event));
			}
			break;
		}
		if (success) {
			for (int i = 0; i < _sp->dot_times; i++) {
				it->event_queue.push_back(CG_event(FyTimerCheckTime(0), _sp->dot_interval * (i+1), -1, _src->stat->_attr, *_sp, &dot_event));
			}
			
			if (_sp->sp_effect != none) {
				OutputDebugString("sp effect pushed\n");
				it->event_queue.push_back(CG_event(FyTimerCheckTime(0), _sp->event_delay, -1, _src->stat->_attr, *_sp, &SPEffect_event));
			}
		}
	}
	return success;
}

inline bool insideCircle(float origin[], float range, float dstPos[]) {
	float dis = sqrt(pow(dstPos[0] - origin[0], 2) + pow(dstPos[1] - origin[1], 2));
	return dis < range;
}
bool insideCone(float srcPos[], float dstPos[], float angle, float range, float dir[]) {
	char msg[256];
	normalize(dir, 2);
	float atk_c = asin((dir[0] * (dstPos[1] - srcPos[1]) - dir[1] * (dstPos[0] - srcPos[0])) / dis(srcPos, dstPos))*180.0f / PI;
	float atk_d = acos((dir[0] * (dstPos[0] - srcPos[0]) + dir[1] * (dstPos[1] - srcPos[1])) / dis(srcPos, dstPos))*180.0f / PI;

	if (dis(srcPos, dstPos) < range && abs(atk_c) <= angle && atk_d <90)
		return true;

	return false;
}
bool insideLine(float srcPos[], float dstPos[], float width, float range, float dir[]) {
	// First check if in range
	float src_into_horizon = dir[0] * srcPos[0] + dir[1] * srcPos[1];	// horizon function ax+by=c

	float divisor = abs((dir[0] * dstPos[0] + dir[1] * dstPos[1] - src_into_horizon));
	float dividend = (sqrt(pow(dir[0], 2) + pow(dir[1], 2)));
	float dis_to_src = divisor / dividend;
	if(dis_to_src > range)
		return false;

	// Then, check in width
	float N[2] = { -dir[1], dir[0] };
	float src_into_axis = N[0] * srcPos[0] + N[1] * srcPos[1];			// axis function ax+by=c

	divisor = abs((N[0] * dstPos[0] + N[1] * dstPos[1] - src_into_axis));
	dividend = (sqrt(pow(N[0], 2) + pow(N[1], 2)));
	float dis_to_axis = divisor / dividend;
	if (dis_to_axis > width)
		return false;

	// Finally, check direction
	float src_dst[2] = {dstPos[0] - srcPos[0], dstPos[1] - srcPos[1]};
	if (src_dst[0] * dir[0] + src_dst[1] * dir[1] < 0)
		return false;

	return true;
}

float targeted_skill(void*src, void*dst, void* sp) {
	char_info	*_src = (char_info*)src;
	char_info	*_dst = (char_info*)dst;
	skill_params*_sp = (skill_params*)sp;
	return 0;
}
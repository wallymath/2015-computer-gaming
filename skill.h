#ifndef __MYSKILL__
#define __MYSKILL__
#include "MyCG_util.h"
#include <iostream>
using namespace std;

struct skill_params{
	SkillType		Stype;
	DirectionType	Dtype;
	GAMEFX_SYSTEMid fxID;
	SPEffect		sp_effect;
	float			sp_value;

	int				dot_times;
	float			dot;
	float			dot_interval;

	float			base;		// base damage
	float			range;		// range
	float			angle;		// for cone 
	float			width;		// for line and circle

	float			skill_delay;	
	float			event_delay;
	float			last;		// lasting time

	float			ap_buf;
	float			ad_buf;

	float			CD;
	float			last_cast;
	float			temp_last_cast;

	char			fx_name[20];
	char			action_name[20];

	skill_params(SkillType stype, DirectionType dtype, float b, float r, float a, float w, float sdelay, float edelay, float l, char* fname, char*aname, float ad_b, float ap_b, float cd, float dt, int dot_time, float d_interval, float spv, GAMEFX_SYSTEMid fID, SPEffect spef) {
		Stype		= stype;
		Dtype		= dtype;
		base		= b;
		range		= r;
		angle		= a;
		width		= w;
		skill_delay = sdelay;
		event_delay = edelay;
		last		= l;
		dot_times	= dot_time;
		dot_interval= d_interval;
		ad_buf		= ad_b;
		ap_buf		= ap_b;
		CD			= cd;
		dot			= dt;
		last_cast	= -1000.f;
		fxID		= fID;
		sp_effect	= spef;
		sp_value	= spv;
		strcpy(fx_name, fname);
		strcpy(action_name, aname);
	}

	void setBackLastCast() {
		last_cast = temp_last_cast;
	}
};

struct skill{
	fptr sk;
	skill_params sp;
	skill(fptr sk, skill_params sp) : sk(sk), sp(sp){};
};

float directional_skill(void*src, void*dst, void* sp);
inline bool insideCircle(float origin[], float range, float dstPos[]);
bool executeDirectionalSkill(void* src, void* dst, void* sp, float mousePos[3], float srcPos[3], float* fxPos);
bool insideCone(float srcPos[], float dstPos[], float angle, float range, float dir[]);
bool insideLine(float srcPos[], float dstPos[], float width, float range, float dir[]);
GAMEFX_SYSTEMid playFX(float fxPos[], bool once, char* name, float last, void* src, void* sp);

#endif
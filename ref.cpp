/*==============================================================
  character movement testing using Fly2

  - Load a scene
  - Generate a terrain object
  - Load a character
  - Control a character to move
  - Change poses
  - demo how to use billboard to create a simple blood bar

  (C)2012-2015 Chuan-Chang Wang, All Rights Reserved
  Created : 0802, 2012

  Last Updated : 1130, 2015, Kevin C. Wang
 ===============================================================*/
#include "FlyWin32.h"

VIEWPORTid vID;                 // the major viewport
SCENEid sID;                    // the 3D scene
OBJECTid cID, tID;              // the main camera and the terrain for terrain following
CHARACTERid actorID;            // the major character
ACTIONid idleID, runID, curPoseID; // two actions
ROOMid terrainRoomID = FAILED_ID;
TEXTid textID = FAILED_ID;

OBJECTid bloodBarObj = FAILED_ID;     // blood bar object id
GEOMETRYid bloodBar01 = FAILED_ID;    // blood bar billboard ID
void AdjustBloodBar(BYTE, BOOL4);
float maxBlood = 20.0f;
float curBlood = 20.0f;

// some globals
int frame = 0;
int oldX, oldY, oldXM, oldYM, oldXMM, oldYMM;

// hotkey callbacks
void QuitGame(BYTE, BOOL4);
void Movement(BYTE, BOOL4);

// timer callbacks
void GameAI(int);
void RenderIt(int);

// mouse callbacks
void InitPivot(int, int);
void PivotCam(int, int);
void InitMove(int, int);
void MoveCam(int, int);
void InitZoom(int, int);
void ZoomCam(int, int);

/*------------------
  the main program
  C.Wang 0720, 2006
 -------------------*/
void FyMain(int argc, char **argv)
{
   // create a new world
   BOOL4 beOK = FyStartFlyWin32("Homework #01 - Use Fly2", 0, 0, 1024, 768, FALSE);

   // setup the data searching paths
   FySetShaderPath("Data\\NTU6\\Shaders");
   FySetModelPath("Data\\NTU6\\Scenes");
   FySetTexturePath("Data\\NTU6\\Scenes\\Textures");
   FySetScenePath("Data\\NTU6\\Scenes");

   // create a viewport
   vID = FyCreateViewport(0, 0, 1024, 768);
   FnViewport vp;
   vp.ID(vID);

   // create a 3D scene
   sID = FyCreateScene(10);
   FnScene scene;
   scene.ID(sID);

   // load the scene
   scene.Load("gameScene02");
   scene.SetAmbientLights(1.0f, 1.0f, 1.0f, 0.6f, 0.6f, 0.6f);

   // load the terrain
   tID = scene.CreateObject(OBJECT);
   FnObject terrain;
   terrain.ID(tID);
   BOOL beOK1 = terrain.Load("terrain");
   terrain.Show(FALSE);

   // set terrain environment
   terrainRoomID = scene.CreateRoom(SIMPLE_ROOM, 10);
   FnRoom room;
   room.ID(terrainRoomID);
   room.AddObject(tID);

   // load the character
   FySetModelPath("Data\\NTU6\\Characters");
   FySetTexturePath("Data\\NTU6\\Characters");
   FySetCharacterPath("Data\\NTU6\\Characters");
   actorID = scene.LoadCharacter("Lyubu2");

   // put the character on terrain
   float pos[3], fDir[3], uDir[3];
   FnCharacter actor;
   actor.ID(actorID);
   pos[0] = 3569.0f; pos[1] = -3208.0f; pos[2] = 1000.0f;
   fDir[0] = 1.0f; fDir[1] = 1.0f; fDir[2] = 0.0f;
   uDir[0] = 0.0f; uDir[1] = 0.0f; uDir[2] = 1.0f;
   actor.SetDirection(fDir, uDir);

   actor.SetTerrainRoom(terrainRoomID, 10.0f);
   beOK = actor.PutOnTerrain(pos);

   // Get character actions
   idleID = actor.GetBodyAction(NULL, "Idle");
   runID = actor.GetBodyAction(NULL, "Run");

   // set the character to idle action
   curPoseID = idleID;
   actor.SetCurrentAction(NULL, 0, curPoseID);
   actor.Play(START, 0.0f, FALSE, TRUE);
   actor.TurnRight(90.0f);

   // translate the camera
   cID = scene.CreateObject(CAMERA);
   FnCamera camera;
   camera.ID(cID);
   camera.SetNearPlane(5.0f);
   camera.SetFarPlane(100000.0f);

   // set camera initial position and orientation
   pos[0] = 4315.783f; pos[1] = -3199.686f; pos[2] = 93.046f;
   fDir[0] = -0.983f; fDir[1] = -0.143f; fDir[2] = -0.119f;
   uDir[0] = -0.116f; uDir[1] = -0.031f; uDir[2] = 0.993f;
   camera.SetPosition(pos);
   camera.SetDirection(fDir, uDir);

   float mainLightPos[3] = { -4579.0f, -714.0f, 15530.0f };
   float mainLightFDir[3] = { 0.276f, 0.0f, -0.961f };
   float mainLightUDir[3] = { 0.961f, 0.026f, 0.276f };

   FnLight lgt;
   lgt.ID(scene.CreateObject(LIGHT));
   lgt.Translate(mainLightPos[0], mainLightPos[1], mainLightPos[2], REPLACE);
	lgt.SetDirection(mainLightFDir, mainLightUDir);
   lgt.SetLightType(PARALLEL_LIGHT);
   lgt.SetColor(1.0f, 1.0f, 1.0f);
   lgt.SetName("MainLight");
   lgt.SetIntensity(0.4f);

   // create a text object
   textID = FyCreateText("Trebuchet MS", 18, FALSE, FALSE);

   // create a billboard object for blood bar
   bloodBarObj = scene.CreateObject(MODEL);
   FnObject model(bloodBarObj);

   float size[2], color[3];
   size[0] = 20.0f;
   size[1] = 2.0f;
   color[0] = 1.0f;
   color[1] = 0.0f;
   color[2] = 0.0f;
   bloodBar01 = model.Billboard(NULL, size, NULL, 0, color);    // the billboard
   
   // get the base object of Lyubu
   OBJECTid baseID = actor.GetBaseObject();

   // make the blood bar's parent is the base and raise up the blood bar higher than the head of lyubu
   model.SetParent(baseID);
   model.Translate(0.0f, 0.0f, 90.0f, REPLACE);

   FyDefineHotKey(FY_F1, AdjustBloodBar, FALSE);    // add blood
   FyDefineHotKey(FY_F2, AdjustBloodBar, FALSE);    // subtract blood

   // set Hotkeys
   FyDefineHotKey(FY_ESCAPE, QuitGame, FALSE);  // escape for quiting the game
   FyDefineHotKey(FY_UP, Movement, FALSE);      // Up for moving forward
   FyDefineHotKey(FY_RIGHT, Movement, FALSE);   // Right for turning right
   FyDefineHotKey(FY_LEFT, Movement, FALSE);    // Left for turning left

   // define some mouse functions
   FyBindMouseFunction(LEFT_MOUSE, InitPivot, PivotCam, NULL, NULL);
   FyBindMouseFunction(MIDDLE_MOUSE, InitZoom, ZoomCam, NULL, NULL);
   FyBindMouseFunction(RIGHT_MOUSE, InitMove, MoveCam, NULL, NULL);

   // bind timers, frame rate = 30 fps
   FyBindTimer(0, 30.0f, GameAI, TRUE);
   FyBindTimer(1, 30.0f, RenderIt, TRUE);

   // invoke the system
   FyInvokeFly(TRUE);
}


/*-----------------------
  add/subtract the blood
  C.Wang 1130, 2015
 ------------------------*/
void AdjustBloodBar(BYTE code, BOOL4 value)
{
   if (!value) {
      float addBlood = 2.0f;

      if (code == FY_F1 || code == FY_F2) {
         if (code == FY_F1) {
            curBlood -= addBlood;
         }
         else {
            curBlood += addBlood;
         }
         if (curBlood < 0.0f) curBlood = 0.0f;
         if (curBlood > maxBlood) curBlood = maxBlood;

         float size[2];
         size[0] = 20.0f*curBlood/maxBlood;
         size[1] = 2.0f;

         FnBillboard bb(bloodBar01);
         bb.SetPositionSize(NULL, size);
      }
   }
}


/*-------------------------------------------------------------
  30fps timer callback in fixed frame rate for major game loop
  C.Wang 1103, 2007
 --------------------------------------------------------------*/
void GameAI(int skip)
{
   FnCharacter actor;

   // play character pose
   actor.ID(actorID);
   actor.Play(LOOP, (float) skip, FALSE, TRUE);

   // Homework #01 part 1
   float dist = 6.0f;

   // for moving forward
   if (FyCheckHotKeyStatus(FY_UP)) {
      actor.MoveForward(dist, TRUE, FALSE, 0.0f, TRUE);
   }

   // for turing left
   if (FyCheckHotKeyStatus(FY_LEFT)) {
      actor.TurnRight(-2.0f);
   }

   // for turning right
   if (FyCheckHotKeyStatus(FY_RIGHT)) {
      actor.TurnRight(2.0f);
   }
}


/*----------------------
  perform the rendering
  C.Wang 0720, 2006
 -----------------------*/
void RenderIt(int skip)
{
   FnViewport vp;

   // render the whole scene
   vp.ID(vID);
   vp.Render3D(cID, TRUE, TRUE);

   // get camera's data
   FnCamera camera;
   camera.ID(cID);

   float pos[3], fDir[3], uDir[3];
   camera.GetPosition(pos);
   camera.GetDirection(fDir, uDir);

   // show frame rate
   static char string[128];
   if (frame == 0) {
      FyTimerReset(0);
   }

   if (frame/10*10 == frame) {
      float curTime;

      curTime = FyTimerCheckTime(0);
      sprintf(string, "Fps: %6.2f", frame/curTime);
   }

   frame += skip;
   if (frame >= 1000) {
      frame = 0;
   }

   FnText text;
   text.ID(textID);

   text.Begin(vID);
   text.Write(string, 20, 20, 255, 0, 0);

   char posS[256], fDirS[256], uDirS[256];
   sprintf(posS, "pos: %8.3f %8.3f %8.3f", pos[0], pos[1], pos[2]);
   sprintf(fDirS, "facing: %8.3f %8.3f %8.3f", fDir[0], fDir[1], fDir[2]);
   sprintf(uDirS, "up: %8.3f %8.3f %8.3f", uDir[0], uDir[1], uDir[2]);

   text.Write(posS, 20, 35, 255, 255, 0);
   text.Write(fDirS, 20, 50, 255, 255, 0);
   text.Write(uDirS, 20, 65, 255, 255, 0);

   text.End();

   // swap buffer
   FySwapBuffers();
}


/*------------------
  movement control
  C.Wang 1103, 2006
 -------------------*/
void Movement(BYTE code, BOOL4 value)
{
   // Homework #01 part 2
   FnCharacter actor;
   actor.ID(actorID);

   // if some key is released
   if (!value) {
      if (code == FY_UP || code == FY_LEFT || code == FY_RIGHT) {
         // if we are running
         if (curPoseID == runID) {
            // and no key are pressing
            if (!FyCheckHotKeyStatus(FY_UP) && !FyCheckHotKeyStatus(FY_LEFT) && !FyCheckHotKeyStatus(FY_RIGHT)) {
               // change the pose to idle
               curPoseID = idleID;
               actor.SetCurrentAction(0, NULL, curPoseID, 5.0f);
               actor.Play(START, 0.0f, FALSE, TRUE);
            }
         }
      }

   }
   else {
      // if some key is pressed
      if (code == FY_UP || code == FY_LEFT || code == FY_RIGHT) {
         // if the character is in idle
         if (curPoseID == idleID) {
            // make he to run
            curPoseID = runID;
            actor.SetCurrentAction(0, NULL, curPoseID, 5.0f);
            actor.Play(START, 0.0f, FALSE, TRUE);
         }
      }
   }
}


/*------------------
  quit the demo
  C.Wang 0327, 2005
 -------------------*/
void QuitGame(BYTE code, BOOL4 value)
{
   if (code == FY_ESCAPE) {
      if (value) {
         FyQuitFlyWin32();
      }
   }
}



/*-----------------------------------
  initialize the pivot of the camera
  C.Wang 0329, 2005
 ------------------------------------*/
void InitPivot(int x, int y)
{
   oldX = x;
   oldY = y;
   frame = 0;
}


/*------------------
  pivot the camera
  C.Wang 0329, 2005
 -------------------*/
void PivotCam(int x, int y)
{
   FnObject model;

   if (x != oldX) {
      model.ID(cID);
      model.Rotate(Z_AXIS, (float) (x - oldX), GLOBAL);
      oldX = x;
   }

   if (y != oldY) {
      model.ID(cID);
      model.Rotate(X_AXIS, (float) (y - oldY), GLOBAL);
      oldY = y;
   }
}


/*----------------------------------
  initialize the move of the camera
  C.Wang 0329, 2005
 -----------------------------------*/
void InitMove(int x, int y)
{
   oldXM = x;
   oldYM = y;
   frame = 0;
}


/*------------------
  move the camera
  C.Wang 0329, 2005
 -------------------*/
void MoveCam(int x, int y)
{
   if (x != oldXM) {
      FnObject model;

      model.ID(cID);
      model.Translate((float)(x - oldXM)*2.0f, 0.0f, 0.0f, LOCAL);
      oldXM = x;
   }
   if (y != oldYM) {
      FnObject model;

      model.ID(cID);
      model.Translate(0.0f, (float)(oldYM - y)*2.0f, 0.0f, LOCAL);
      oldYM = y;
   }
}


/*----------------------------------
  initialize the zoom of the camera
  C.Wang 0329, 2005
 -----------------------------------*/
void InitZoom(int x, int y)
{
   oldXMM = x;
   oldYMM = y;
   frame = 0;
}


/*------------------
  zoom the camera
  C.Wang 0329, 2005
 -------------------*/
void ZoomCam(int x, int y)
{
   if (x != oldXMM || y != oldYMM) {
      FnObject model;

      model.ID(cID);
      model.Translate(0.0f, 0.0f, (float)(x - oldXMM)*10.0f, LOCAL);
      oldXMM = x;
      oldYMM = y;
   }
}
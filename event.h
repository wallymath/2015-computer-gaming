#ifndef __MYEVENT__
#define __MYEVENT__
#include "MyCG_util.h"
#include "MyCG.h"
#include "skill.h" 
#include <iostream>

float reverseStatus(void* src, void* dst, void* sp);


float point_atk_event(void*src, void*dst, void* sp){
	char_attr	*_src	= (char_attr*)src;
	char_info	*_dst	= (char_info*)dst;
	skill_params*_sp	= (skill_params*)sp;

	if (_sp->Stype == damage)
		_dst->adjust_blood(-1.f * (_sp->base + _src->_AD.to_float() * _sp->ad_buf + _src->_AP.to_float() * _sp->ap_buf)) ;
	else 
		_dst->adjust_blood(_sp->base + _src->_AD.to_float() * _sp->ad_buf + _src->_AP.to_float() * _sp->ap_buf);
	return 0;
}

float dot_event(void*src, void*dst, void* sp){
	OutputDebugString("inside dot event!\n");
	char_attr	*_src = (char_attr*)src;
	char_info	*_dst = (char_info*)dst;
	skill_params*_sp = (skill_params*)sp;

	float ratio = _sp->dot / _sp->base;
	float amount = _sp->dot + (_src->_AD.to_float() * _sp->ad_buf + _src->_AP.to_float() * _sp->ap_buf) * ratio;
	if (_sp->Stype == damage)
		_dst->adjust_blood(-1.f * amount);
	else
		_dst->adjust_blood(amount);

	// Need to play dot animation 
	return 0;
}

float SPEffect_event(void*src, void*dst, void* sp) {
	OutputDebugString("inside speffect_event\n");
	char_attr	*_src = (char_attr*)src;
	char_info	*_dst = (char_info*)dst;
	skill_params*_sp = (skill_params*)sp;
	switch (_sp->sp_effect) {
		float curPos[3];
		// Confined Object
		case stunned:
			_dst->actor.GetPosition(curPos);
			
			_dst->active = std::queue<CG_job>();
			_dst->active.push(CG_job(new float[]{curPos[0], curPos[1], curPos[2]}, NULL, -1.0, JobType::move, NULL));
			_dst->act_job = 0;
			_dst->updatePoseID = _dst->idleID;
			_dst->flag.insert(stunned);

			playFX(curPos, false, "456787", _sp->sp_value, _dst, _sp);
			break;

		case snared:
			_dst->actor.GetPosition(curPos);

			OutputDebugString("inside snared\n");
			_dst->active = std::queue<CG_job>();
			_dst->active.push(CG_job(new float[]{curPos[0], curPos[1], curPos[2]}, NULL, -1.0, JobType::move, NULL));
			_dst->act_job = 0;
			_dst->updatePoseID = _dst->idleID;
			_dst->flag.insert(snared);

			playFX(curPos, false, "MagicCircle20ori", _sp->sp_value, _dst, _sp);
			break;

		case silent:
			float* origin_CD = (float*)malloc(sizeof(float)* 4);
			for (int i = 0; i < 4; i++) {
				origin_CD[i] = _dst->_skillSet[i].sp.last_cast;
				_dst->_skillSet[i].sp.temp_last_cast = _dst->_skillSet[i].sp.last_cast;
				_dst->_skillSet[i].sp.last_cast = FyTimerCheckTime(0) + 10000.f;
			}
			break;
	}
	_dst->event_queue.push_back(CG_event(FyTimerCheckTime(0), _sp->sp_value, -1, *_src, *_sp, &reverseStatus));
	return 0.f;
}

float reverseStatus(void* src, void* dst, void* sp) {
	char_attr	*_src = (char_attr*)src;
	char_info	*_dst = (char_info*)dst;
	skill_params*_sp = (skill_params*)sp;

	if (_sp->sp_effect == silent) {
		for (int i = 0; i < 4; i++) {
			_dst->_skillSet[i].sp.setBackLastCast();
		}
	}
	_dst->flag.erase(_sp->sp_effect);
	return 0.f;
}

float remove_fx(void* src, void* dst, void* sp) {
	char_attr	*_src = (char_attr*)src;
	char_info	*_dst = (char_info*)dst;
	skill_params*_sp = (skill_params*)sp;

	extern std::vector<Effect> FXs;
	extern SCENEid sID;

	FnScene scene(sID);
	scene.DeleteGameFXSystem(_sp->fxID);

	int index = -1;

	for (int i = 0; i < FXs.size(); i++) {
		if (FXs[i].FXID == _sp->fxID)
			index = i;
	}
	char msg[256];
	if (index == -1)
		return 0.f;
	FXs.erase(FXs.begin()+index);
	return 0.f;
}

float delay_skill(void* src, void* dst, void* sp) {
	char_attr	*_src = (char_attr*)src;
	char_info	*_dst = (char_info*)dst;
	skill_params*_sp = (skill_params*)sp;

	float fxPos[3];
	bool result = executeDirectionalSkill(_dst, NULL, _sp, _src->mouseThen, _src->posThen, fxPos);
	return result;
}
#endif
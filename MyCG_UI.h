#ifndef __MYCGUI__
#define __MYCGUI__
#include <math.h>
#include <cstdio>
#include "MyCG.h"
#include "FlyWin32.h"
#include "FyMedia.h"
extern float w, h;
bool RenderMenu(SCENEid spsID, OBJECTid* MenuID, OBJECTid* buttonID1, OBJECTid* BGMID){
	
	char buf[MAX_PATH];
	FnScene scene;
	scene.ID(spsID);
	scene.SetSpriteWorldSize(w, h);
	FnSprite sp;
	sprintf(buf, "%sNowOrNever.mp3", UIPath);
	*BGMID = FyCreateMediaPlayer(buf, 0, 0, 800, 600);
	FnMedia mP;
	mP.Object(*BGMID);
	mP.Play(LOOP);
	*MenuID = scene.CreateObject(SPRITE);
	sp.ID(*MenuID);
	sp.SetSize(w, h);
	sprintf(buf, "%sWelcome", UIPath);
	sp.SetImage(buf, 0, NULL, 0, NULL, NULL, MANAGED_MEMORY, FALSE, FALSE);
	sp.SetPosition(0, 0, 0);

	*buttonID1 = scene.CreateObject(SPRITE);
	sp.ID(*buttonID1);
	sp.SetSize(300, 100);
	sprintf(buf, "%sstart_button", UIPath);
	sp.SetImage(buf, 0, NULL, 0, NULL, NULL, MANAGED_MEMORY, FALSE, FALSE);
	sp.SetPosition(5, 10, 1);
	OutputDebugString("Menu Rendered\n");
	return true;
}

bool RenderUI(std::vector<char_info*>selected_chars, FnText text){
	if (selected_chars.size() != 0){
		auto mainChar = selected_chars[0];
		char buf[30];	
		// Print Blood
		sprintf(buf, "%.0f  /  %.0f\n", mainChar->stat->cur_blood.to_float(), mainChar->stat->_max_blood);
		text.Write(buf, 120, 453, 0, 0, 0);
		// Print MP
		sprintf(buf, "%.0f  /  %.0f\n", mainChar->stat->cur_MP.to_float(), mainChar->stat->_max_MP);
		text.Write(buf, 120, 490, 0, 0, 0);
		// Print AD
		sprintf(buf, "%.2f\n", mainChar->stat->_attr._AD.to_float());
		text.Write(buf, 300, 435, 0, 0, 0);
		// Print AP
		sprintf(buf, "%.2f\n", mainChar->stat->_attr._AP.to_float());
		text.Write(buf, 300, 465, 0, 0, 0);
		// Print speed
		sprintf(buf, "%.2f\n", mainChar->stat->_attr._speed.to_float());
		text.Write(buf, 300, 495, 0, 0, 0);
	}
	return true;
}

bool UIInit(SCENEid spsID, OBJECTid MenuID, OBJECTid buttonID1, OBJECTid& UIID, OBJECTid& headImageID, OBJECTid skillImageID[]){
	char buf[MAX_PATH];
	FnScene scene;
	scene.ID(spsID);
	scene.DeleteObject(MenuID);
	scene.DeleteObject(buttonID1);
	FnSprite sp;
	UIID = scene.CreateObject(SPRITE);
	sp.ID(UIID);
	sp.SetSize(w, 230);
	sprintf(buf, "%sUI0", UIPath);
	sp.SetImage(buf, 0, NULL, 0, NULL, NULL, MANAGED_MEMORY, FALSE, FALSE);
	sp.SetPosition(0, 0, 1);
	headImageID = scene.CreateObject(SPRITE);
	sp.ID(headImageID);
	sp.SetSize(70, 70);
	sp.SetPosition(21, 71, 2);
	for (int i = 0; i < SKILL_NUM; i++){
		skillImageID[i] = scene.CreateObject(SPRITE);
		sp.ID(skillImageID[i]);
		sp.SetSize(35, 35);
		sp.SetPosition(28+55*i, 16, 2);
	}
	return true;
}

bool UpdateUI(vector<char_info*> selected_chars, OBJECTid headImageID, OBJECTid skillImageID[]){
	char buf[MAX_PATH];
	FnSprite sp;
	sp.ID(headImageID);
	OutputDebugString("In UI\n");
	char debug[256];
	if ((selected_chars).size() != 0){
		sprintf(debug, "size: %d\n", (selected_chars).size());
		OutputDebugString(debug);
		auto firstChar = selected_chars[0];
		OutputDebugString("0.5\n");
		sprintf(buf, "%d\n", firstChar->actorID);
		OutputDebugString(buf);
		sprintf(buf, "%s%s\\head", UIPath,firstChar->char_name);
		OutputDebugString("1\n");
		sp.SetImage(buf, 0, NULL, 0, NULL, NULL, MANAGED_MEMORY, FALSE, FALSE);
		OutputDebugString("2\n");
		for (int i = 0; i < SKILL_NUM; i++){
			sp.ID(skillImageID[i]);
			sprintf(buf, "%s%s\\skill0%d", UIPath, firstChar->char_name, i);
			sp.SetImage(buf, 0, NULL, 0, NULL, NULL, MANAGED_MEMORY, FALSE, FALSE);
		}
	}
	else{
		sp.SetImage(NULL, 0, NULL, 0, NULL, NULL, MANAGED_MEMORY, FALSE, FALSE);
		for (int i = 0; i < SKILL_NUM; i++){
			sp.ID(skillImageID[i]);
			sp.SetImage(NULL, 0, NULL, 0, NULL, NULL, MANAGED_MEMORY, FALSE, FALSE);
		}
	}
	OutputDebugString("Out UI\n");
	return true;
}
#endif
/*==============================================================
  character movement testing using Fly2

  - Load a scene
  - Generate a terrain object
  - Load a character
  - Control a character to move
  - Change poses

  (C)2012-2015 Chuan-Chang Wang, All Rights Reserved
  Created : 0802, 2012

  Last Updated : 1004, 2015, Kevin C. Wang
 ===============================================================*/
#include "MyCG_limit.h"
#include "MyCG_util.h"
#include "MyCG.h"
#include "MyCG_stage.h" 
#include "MyCG_findpath.h"
//#include "MyCG_saveload.h"
#include "MyCG_UI.h"
#include "FlyWin32.h"
#include <math.h>
#include <cstdio>
#include <set>
#include <math.h>       /* acos */


#define PI 3.14159265
#define TRACK_UPPER 900
#define TRACK_LOWER 700
#define H_UPPER 700.0
#define H_LOWER 300.0
#define NORM_ATK_DIS 180.0s
extern "C" const float PTSELECTBOX[] = { 30.0f, 30.0f, 10.0f, 55.0f };


/* Selection Code */
// selection "square"
BOOL4 beSelect = FALSE;
SCENEid s2DID = FAILED_ID;
OBJECTid selectionBoxID = FAILED_ID;
OBJECTid selectCamID = FAILED_ID;

// For testing FX
int playMethod = 2;
OBJECTid dummyID = FAILED_ID;
GAMEFX_SYSTEMid gFXID = FAILED_ID;

//static int current_gridmap_status[gridmap_x][gridmap_y];
int current_gridmap_status[MAX_GRID_X][MAX_GRID_Y];
float gridmap_cost[MAX_GRID_X][MAX_GRID_Y];

VIEWPORTid vID;                 // the major viewport
SCENEid sID, spsID;             // the 3D scene
OBJECTid cID, tID;              // the main camera and the terrain for terrain following
OBJECTid MenuID, buttonID1, UIID, headImageID, BGMID;
OBJECTid skillImageID[SKILL_NUM];
ROOMid terrainRoomID = FAILED_ID;
TEXTid textID = FAILED_ID;
GameStatus GS;		//Indicates in menu, gaming, save ...

char_info Lyubu, Robber;
std::vector<char_info> all_char;

// some globals
float mouseX, mouseY, mouseZ ;
float screenX, screenY;
float rotAxis[3];

std::vector<Effect> FXs;
std::vector<char_info*> selected_chars;
std::vector<char_info*> prev_selected_chars;

float w = 1024.f, h = 576.f;
float cpos[3], cfDir[3], cuDir[3];
int frame = 0;
int oldX, oldY, oldXM, oldYM, oldXMM, oldYMM;
int outFlag = FALSE;
int BodyID;

void updateAllSelectedChar(float* v2d, int vLen, const float tol[4]);

bool insideRect(float** vertex, float* charPos);
void toggleCharShader();
void applyShader(CHARACTERid id, char* name);

// hotkey callbacks
void QuitGame(BYTE, BOOL4);
void Combat(BYTE, BOOL4);

// timer callbacks
void GameAI(int);
void RenderIt(int);

// mouse callbacks
void RClick(int, int);
void PivotCam(int, int);
void InitMove(int, int);
void MoveCam(float, float);
void Click(int, int);
void Select(int, int);
void Release(int, int);
void InitZoom(int, int);
void ZoomCam(int, int);
void CamMoveCheck(int x, int y);
void test(int, int);
void cast_skill(char_info* who, int idx);

/*------------------
  the main program
  C.Wang 1010, 2014
 -------------------*/
void FyMain(int argc, char **argv)
{
	// create a new world
	BOOL4 beOK = FyStartFlyWin32("NTU@2014 Homework #01 - Use Fly2", 0, 0, w, h, FALSE);

	// setup the data searching paths
	FySetShaderPath("Data\\NTU6\\Shaders");
	FySetModelPath("Data\\NTU6\\Scenes");
	FySetTexturePath("Data\\NTU6\\Scenes\\Textures");
	FySetScenePath("Data\\NTU6\\Scenes");
	FySetGameFXPath("Data\\NTU6\\FX");

	// create a viewport
	vID = FyCreateViewport(0, 0, w, h);
	FnViewport vp;
	vp.ID(vID);

	// create a 3D scene
	sID = FyCreateScene(10);
	FnScene scene;
	scene.ID(sID);

	// load the scene
	scene.Load("gameScene02");
	scene.SetAmbientLights(1.0f, 1.0f, 1.0f, 0.6f, 0.6f, 0.6f);

	// load the terrain
	tID = scene.CreateObject(OBJECT);
	FnObject terrain;
	terrain.ID(tID);
	BOOL beOK1 = terrain.Load("terrain");
	terrain.Show(FALSE);
	
	// set terrain environment
	terrainRoomID = scene.CreateRoom(SIMPLE_ROOM, 10);
	FnRoom room;
	room.ID(terrainRoomID);
	room.AddObject(tID);

	// load the character
	FySetModelPath("Data\\NTU6\\Characters");
	FySetTexturePath("Data\\NTU6\\Characters");
	FySetCharacterPath("Data\\NTU6\\Characters");

	// put the character on terrain

	FyTimerReset(0);
	float pos[3] = { 3569.0f, -3208.0f, 0.0f }, fDir[3] = { 1.0f, 1.0f, 0.0f }, uDir[3] = { 0.0f, 0.0f, 1.0f };
	FnCharacter actor;
	load_stage(0, terrainRoomID, tID, sID, all_char, current_gridmap_status, gridmap_cost);
	
	FnCamera camera;
	// translate the camera
	cID = scene.CreateObject(CAMERA);
   
	camera.ID(cID);
	camera.SetNearPlane(5.0f);
	camera.SetFarPlane(100000.0f);
	// set camera initial position and orientation
   
	cpos[0] = 3547.5f; cpos[1] = -4208.0f; cpos[2] = 1000.0f;
	cfDir[0] = pos[0]-cpos[0]; cfDir[1] = pos[1]-cpos[1]; cfDir[2] = pos[2]-cpos[2];
	cuDir[0] = 0.0f; cuDir[1] = 0.0f; cuDir[2] = 1.0f;
	
	camera.SetPosition(cpos);
	//camera.SetDirection(NULL, cuDir);
	camera.SetDirection(cfDir, NULL);
    
	float mainLightPos[3]	= { -4579.0, -714.0, 15530.0 };
	float mainLightFDir[3]	= { 0.276, 0.0, -0.961 };
	float mainLightUDir[3]	= { 0.961, 0.026, 0.276 };
   
	FnLight lgt;
	lgt.ID(scene.CreateObject(LIGHT));
	lgt.Translate(mainLightPos[0], mainLightPos[1], mainLightPos[2], REPLACE);
	lgt.SetDirection(mainLightFDir, mainLightUDir);
	lgt.SetLightType(PARALLEL_LIGHT);
	lgt.SetColor(1.0f, 1.0f, 1.0f);
	lgt.SetName("MainLight");
	lgt.SetIntensity(0.4f);

	// create a text object for displaying messages on screen
	textID = FyCreateText("Trebuchet MS", 18, FALSE, FALSE);

	// set Hotkeys
	FyDefineHotKey(FY_ESCAPE, QuitGame, FALSE);  // escape for quiting the game

   // Combat Keys
   FyDefineHotKey(FY_Q, Combat, FALSE);
   FyDefineHotKey(FY_W, Combat, FALSE);
   FyDefineHotKey(FY_E, Combat, FALSE);
   FyDefineHotKey(FY_R, Combat, FALSE);

	// define some mouse functions
	FyBindMouseMoveFunction(test);
	FyBindMouseFunction(LEFT_MOUSE, Click, Select, Release, NULL);
	FyBindMouseFunction(RIGHT_MOUSE, RClick, PivotCam, NULL, NULL);	
	FyBindMouseFunction(MIDDLE_MOUSE, InitZoom, ZoomCam, NULL, NULL);
	//FyBindMouseFunction(RIGHT_MOUSE, InitMove, MoveCam, NULL, NULL);

	// bind timers, frame rate = 30 fps
	FyBindTimer(0, 30.0f, GameAI, TRUE);
	FyBindTimer(1, 30.0f, RenderIt, TRUE);

	// Init rotAxis
	float result[3];

	screenX = w / 2; screenY = h / 2;
	int success = vp.HitPosition(tID, cID, screenX, screenY, result);
	if (success != FAILED_ID) {
		mouseX = result[0];
		mouseY = result[1];
		mouseZ = result[2];
	}
	rotAxis[0] = result[0]; rotAxis[1] = result[1]; rotAxis[2] = result[2];

	/* Selection Box */
	// create the "square" object and an orthogal camera to draw the selection region
	s2DID = FyCreateScene();
	scene.ID(s2DID);

	selectionBoxID	= scene.CreateObject(MODEL);
	selectCamID		= scene.CreateObject(CAMERA);

	FnCamera orthCam(selectCamID);
	orthCam.SetProjectionType(ORTHOGONAL);				// set the camera in orthogonal projection
	orthCam.Translate(0.0f, 0.0f, 100.0f, REPLACE);		// move it higher than the square object
	orthCam.SetAspectRatio(1.0f);						// aspect ratio = 1.0

	// construct the "square" using Lines with green color : 5 vertices in (x, y, z, r, g, b) each
	FnObject selectBox(selectionBoxID);
	MATERIALid mID = FyCreateMaterial();
	float v2d[30], color[3];

	color[0] = 0.f; color[1] = 0.f; color[2] = 0.f;   // green color
		
	v2d[0] = -0.0f;		v2d[1] = 0.0f;		v2d[2] = 0.0f;
	v2d[3] = color[0];	v2d[4] = color[1];	v2d[5] = color[2];
	v2d[6] = -0.0f;		v2d[7] = -0.0f;		v2d[8] = 0.0f;
	v2d[9] = color[0];	v2d[10] = color[1];	v2d[11] = color[2];
	v2d[12] = 0.0f;		v2d[13] = -0.0f;	v2d[14] = 0.0f;
	v2d[15] = color[0]; v2d[16] = color[1]; v2d[17] = color[2];
	v2d[18] = 0.0f;		v2d[19] = 0.0f;		v2d[20] = 0.0f;
	v2d[21] = color[0];	v2d[22] = color[1]; v2d[23] = color[2];
	v2d[24] = -0.4f;	v2d[25] = 0.4f;		v2d[26] = 0.0f;
	v2d[27] = color[0]; v2d[28] = color[1]; v2d[29] = color[2];
	selectBox.Lines(OPEN_POLYLINE, mID, v2d, 5, TRUE, TRUE);

	// hide the selection box as initial
	selectBox.Show(FALSE);
	beSelect = FALSE;
	
    spsID = FyCreateScene(1);
	RenderMenu(spsID, &MenuID, &buttonID1, &BGMID);
	// invoke the system
	FyInvokeFly(TRUE);
}


/*-------------------------------------------------------------
  30fps timer callback in fixed frame rate for major game loop
 --------------------------------------------------------------*/
void GameAI(int skip)
{
	if (GS!=Game)
		return;
	std::vector<char_info>::iterator it;
	FnCharacter actor;

	// play game FX
	if (gFXID != FAILED_ID) {
		FnGameFXSystem gxS(gFXID);
		BOOL4 beOK = gxS.Play((float)skip, ONCE);
		if (!beOK) {
			FnScene scene(sID);
			scene.DeleteGameFXSystem(gFXID);
			gFXID = FAILED_ID;
		}
	}

	for (it = all_char.begin(); it != all_char.end(); it++){	// enumerate all characters
		char msg[256];
		
		actor.ID(it->actorID);

		float actorPos[3], actorDir[3];
		std::list<CG_event>::iterator itt = it->event_queue.begin();
		while (itt != it->event_queue.end()){//deal w/ buf effect
			bool del_flag = false;
			if (FyTimerCheckTime(0) - (itt->insert) > (itt->delay)){// Start execute
				if (itt->last == -1){	//if no last, exec and remove
					itt->callback((void*)&(itt->atk_attr), (void*)&(*it), (void*)&(itt->atk_params));
					itt = it->event_queue.erase(itt);
					del_flag = true;
				}
			}
			if (!del_flag)
				itt++;
		}
		//effect
		// Guard -> transform job queue
		if (it->curPoseID == it->dieID){	// if dead, no job
			int i = 0;
			for (; i < selected_chars.size(); i++)
				if (*it == *selected_chars[i])
					break;
			if (i != selected_chars.size())
				selected_chars.erase(selected_chars.begin() + i);
		}
		else{
			if (it->camp != p1 && it->camp != p2)
				it->AI(&(*it), &all_char, NULL);
			CG_job* cur_job;

			if (it->act_job)
				cur_job = &(it->routine[it->job_ptr]);
			else{
				cur_job = &(it->active.front());
			}
			actor.GetPosition(actorPos);
			float dis_move = dis2D(actorPos, cur_job->dst);
			switch (cur_job->type){
			case  JobType::move:	// move to specific location
				if (dis_move < MIN_DIS){	//Job DONE
					if (cur_job->done == -1){	//First frame after job
						cur_job->done = FyTimerCheckTime(0);
						OutputDebugString("move job done\n");
					}
					if (FyTimerCheckTime(0) - cur_job->done > cur_job->last && cur_job->last != -1){
						it->updatePoseID = it->runID;
						if (it->act_job)	//character in routine mode, run next routine
							it->job_ptr = (it->job_ptr + 1) % (it->routine.size());
						else{	//active mode
							it->active.pop();
							if (it->active.empty())	//if noting left, back to routine
								it->act_job = 1;
						}
					}
					else{
						it->updatePoseID = it->guardID;
					}
				}
				else{
					actor.SetDirection(new float[3]{cur_job->dst[0] - actorPos[0], cur_job->dst[1] - actorPos[1], cur_job->dst[2] - actorPos[2]}, new float[3]{0.0f, 0.0f, 1.0f});
					if (dis_move < it->stat->_attr._speed.to_float())	//dis_left smaller than char speed, move to target
						actor.MoveForward(dis_move, TRUE, FALSE, 0.0f, TRUE);
					else
						actor.MoveForward(it->stat->_attr._speed.to_float(), TRUE, FALSE, 0.0f, TRUE);
				}
				break;
			case  JobType::move_attack:
				break;
			case  JobType::attack:
				float apos[3], tpos[3], dis2target;
				char_info* t = (char_info*)cur_job->target;
				if (cur_job->target != NULL){	//try to attack on target
					it->actor.GetPosition(apos);
					t->actor.GetPosition(tpos);
					dis2target = dis2D(apos, tpos);
					if (dis2target < cur_job->job_sk->sp.range){	//If enemy in attack range, attack until next command
						it->stat->_attr.mouseThen = tpos;
						if (cur_job->job_sk->sk(&(*it), cur_job->target, &(cur_job->job_sk->sp)))
							it->updatePoseID = it->skID[0];
						if (it->active.empty())	//if noting left, back to routine
							it->act_job = 1;
					}
					else{	// If enemy not in attack range, run to him and attack
						if (cur_job->dst[0] == -1){	//Not yet execute findpath, findpath and fill active queue
							findpath(*it, tpos, JobType::attack, t, cur_job->job_sk);
							it->active.push(CG_job(new float[3]{-1, -1, -1}, (void*)t, -1.0, JobType::attack, cur_job->job_sk));
						}
						else{
							actor.SetDirection(new float[3]{cur_job->dst[0] - actorPos[0], cur_job->dst[1] - actorPos[1], cur_job->dst[2] - actorPos[2]}, new float[3]{0.0f, 0.0f, 1.0f});
							if (dis_move < it->stat->_attr._speed.to_float())	//dis_left smaller than char speed, move to target
								actor.MoveForward(dis_move, TRUE, FALSE, 0.0f, TRUE);
							else
								actor.MoveForward(it->stat->_attr._speed.to_float(), TRUE, FALSE, 0.0f, TRUE);
						}
					}
				}
				else{	//try to attack on terrain
					if (dis_move < cur_job->job_sk->sp.range){
						it->stat->_attr.mouseThen = new float[3]{mouseX, mouseY, mouseZ};
						if (it->active.size() == 1){	//final destination
							cur_job->job_sk->sk(&(*it), cur_job->target, &(cur_job->job_sk->sp));
							it->updatePoseID = it->skID[0];
							if (it->active.empty())	//if noting left, back to routine
								it->act_job = 1;
						}
						else{
							it->updatePoseID = it->runID;
							it->active.pop();
							if (it->active.empty())	//if noting left, back to routine
								it->act_job = 1;
						}
					}
					else{
						actor.SetDirection(new float[3]{cur_job->dst[0] - actorPos[0], cur_job->dst[1] - actorPos[1], cur_job->dst[2] - actorPos[2]}, new float[3]{0.0f, 0.0f, 1.0f});
						if (dis_move < it->stat->_attr._speed.to_float())	//dis_left smaller than char speed, move to target
							actor.MoveForward(dis_move, TRUE, FALSE, 0.0f, TRUE);
						else
							actor.MoveForward(it->stat->_attr._speed.to_float(), TRUE, FALSE, 0.0f, TRUE);
					}
				}

				break;
			}
		}
		it->update_pos();
		if (isLoop(&(*it), it->curPoseID))
			actor.Play(LOOP, (float)skip, FALSE, TRUE);
		else if (!actor.Play(ONCE, (float)skip, FALSE, TRUE)){
			if (it->curPoseID!=it->dieID)
				it->updatePoseID = it->guardID;
		}
	}
}

/*----------------------
  perform the rendering
  C.Wang 0720, 2006
 -----------------------*/
void RenderIt(int skip)
{
	FnViewport vp;

	// render the whole scene
	vp.ID(vID);
	vp.Render3D(cID, TRUE, TRUE);
	vp.RenderSprites(spsID, FALSE, TRUE);
	// get camera's data
	FnCamera camera;
	camera.ID(cID);

	float pos[3], fDir[3], uDir[3];
	camera.GetPosition(pos);
	camera.GetDirection(fDir, uDir);

	// show frame rate
	static char string[128];
	if (frame == 0) {
		FyTimerReset(0);
	}

	if (frame / 10 * 10 == frame) {
		float curTime;

		curTime = FyTimerCheckTime(0);
		sprintf(string, "Fps: %6.2f", frame / curTime);
	}

	frame += skip;
	if (frame >= 100000) {
		frame = 0;
	}

	if (beSelect) {
		vp.Render3D(selectCamID, FALSE, TRUE);
	}

	char tmp[256];
	sprintf(tmp, "#FXs: %d \n", FXs.size());
	// Render all FXs
	std::vector<Effect> index;
	for (auto it = FXs.begin(); it != FXs.end(); it++){
		FnGameFXSystem gxS(it->FXID);
		BOOL4 beOK;
		if (it->once)
			beOK = gxS.Play((float)skip, ONCE);
		else
			beOK = gxS.Play((float)skip, LOOP);

		if (!beOK) {
			FnScene scene(sID);
			scene.DeleteGameFXSystem(it->FXID);
			index.push_back(*it);
		}
	}
	for (auto it = index.begin(); it != index.end(); it++) {
		FXs.erase(std::remove(FXs.begin(), FXs.end(), *it), FXs.end());
	}

	FnText text;
	text.ID(textID);
	RenderUI(selected_chars, text);
	text.End();
	CamMoveCheck(screenX, screenY);
	// swap buffer
	FySwapBuffers();
}

void Combat(BYTE code, BOOL4 value)
{
	if (!selected_chars.size())
		return;

	char_info* first_char = selected_chars[0];
	switch (code) {
		case FY_Q:
			if (first_char->_skillSet[1].sk == NULL) break;

			first_char->stat->_attr.mouseThen = new float[3]{mouseX, mouseY, mouseZ};
			cast_skill(first_char, 1);
			break;
		case FY_W:
			if (first_char->_skillSet[2].sk == NULL) break;

			first_char->stat->_attr.mouseThen = new float[3]{mouseX, mouseY, mouseZ};
			cast_skill(first_char, 2);
			break;
		case FY_E:
			if (first_char->_skillSet[3].sk == NULL) break;

			first_char->stat->_attr.mouseThen = new float[3]{mouseX, mouseY, mouseZ};
			cast_skill(first_char, 3);
			break;
		case FY_R:
			break;
	}
}

void cast_skill(char_info* who, int idx){
	who->stat->_attr.mouseThen = new float[3]{mouseX, mouseY, mouseZ};
	if (who->_skillSet[idx].sp.Dtype == target){	//target, cast on player
		bool find_enemy = false;
		FnViewport vp;
		vp.ID(vID);
		float sPos[2], tPos[3];
		for (auto itt = all_char.begin(); itt != all_char.end(); ++itt) {
			if (checkEnemy(who->camp, itt->camp) || !itt->canDamage)		//not enemy or is dead
				continue;
			itt->actor.GetPosition(tPos);
			vp.ComputeScreenPosition(cID, sPos, tPos, PHYSICAL_SCREEN, FALSE);
			float sx = (float)who->stat->_attr.mouseThen[0] / w - 0.5f;
			float  sy = 1.0f - (float)who->stat->_attr.mouseThen[1] / h - 0.5f;
			float** vertex = new float*[4];
			for (int i = 0; i < 4; i++) {
				vertex[i] = new float[2];
				vertex[i][0] = (sx + 0.5) * w;
				vertex[i][0] += i < 2 ? (-1)*PTSELECTBOX[0] : PTSELECTBOX[1];
				vertex[i][1] = -(sy - 0.5) * h;
				vertex[i][1] += (i == 0 || i == 3) ? (-1)*PTSELECTBOX[2] : PTSELECTBOX[3];
			}
			bool result = insideRect(vertex, sPos);
			if (result) {
				who->active = std::queue<CG_job>();
				who->active.push(CG_job(new float[3]{-1, -1, -1}, (void*)&(*itt), -1.0, JobType::attack, &(who->_skillSet[idx])));
				find_enemy = true;
				break;
			}
		}
	}
	else{	//directional, cast on terrain
		who->active = std::queue<CG_job>();
		who->active.push(CG_job(who->stat->_attr.mouseThen, NULL, -1.0, JobType::attack, &(who->_skillSet[idx])));
	}
	who->act_job = 0;
	who->updatePoseID = who->runID;
}


/*------------------
  quit the demo
  C.Wang 0327, 2005
 -------------------*/
void QuitGame(BYTE code, BOOL4 value)
{
	/*
   if (code == FY_ESCAPE) {
      if (value) {
         FyQuitFlyWin32();
      }
   }
   */
}

/*-----------------------------------
  initialize the pivot of the camera
  C.Wang 0329, 2005
 ------------------------------------*/
void RClick(int x, int y)
{
	switch (GS){
	case MainMenu:
		break;
	case Game:
		oldX = x;
		oldY = y;
		FnViewport vp;
		float* xyz = (float*)malloc(sizeof(float)*3);

		// hit test the terrain
		vp.ID(vID);
		GEOMETRYid gID = vp.HitPosition(tID, cID, x, y, xyz);
		if (gID == FAILED_ID)	return;
		if (FyCheckHotKeyStatus(FY_SHIFT)) {

			if (gID != FAILED_ID) {
				FnScene scene(sID);

				// remove the old one if necessary
				if (gFXID != NULL) {
					scene.DeleteGameFXSystem(gFXID);
				}

				// create a new game FX system
				gFXID = scene.CreateGameFXSystem();

				// case 1 : we create/move a dummy object on the hit position
				FnGameFXSystem gxS(gFXID);

				// play the FX on it
				BOOL4 beOK = gxS.Load("SpellHome_01", TRUE);
				if (beOK) {
					gxS.SetPlayLocation(xyz);
				}
			}
		}
		else{
			for (auto it = selected_chars.begin(); it != selected_chars.end(); it++){
				// First see if click on enemy

				float** vertex = new float*[4];
				for (int i = 0; i < 4; i++) {
					vertex[i] = new float[2];
					vertex[i][0] = x;
					vertex[i][0] += i < 2 ? (-1)*PTSELECTBOX[0] : PTSELECTBOX[1];
					vertex[i][1] = y;
					vertex[i][1] += (i == 0 || i == 3) ? (-1)*PTSELECTBOX[2] : PTSELECTBOX[3];
				}

				float tPos[3], sPos[2];
				bool find_enemy = false;
				for (auto itt = all_char.begin(); itt != all_char.end(); ++itt) {
					if (itt->camp != enemy || !itt->canDamage)		//not enemy or is dead
						continue;
					itt->actor.GetPosition(tPos);
					vp.ComputeScreenPosition(cID, sPos, tPos, PHYSICAL_SCREEN, FALSE);

					bool result = insideRect(vertex, sPos);
					if (result) {
						(*it)->active = std::queue<CG_job>();
						(*it)->active.push(CG_job(new float[3]{-1,-1,-1}, (void*)&(*itt), -1.0, JobType::attack, &((*it)->_skillSet[0])));
						find_enemy = true;
						break;
					}
				}
				if (!find_enemy)
					findpath(**it, xyz, JobType::move);
				(*it)->act_job = 0;
				(*it)->updatePoseID = (*it)->runID;
			}
		}
		break;
	}
}


/*------------------
  pivot the camera
  C.Wang 0329, 2005
 -------------------*/
void PivotCam(int x, int y)
{
	FnObject model;

   if (x != oldX) {
      model.ID(cID);
	  float angle = (x - oldX) * PI / 180;
	  float fDir[3], pos[3], temp[3] = { 0.f, 0.f, 1.f };

	  model.Translate(-rotAxis[0], -rotAxis[1], 0, GLOBAL);
      model.Rotate(Z_AXIS, (float) (x - oldX), GLOBAL);
	  model.Translate(rotAxis[0], rotAxis[1], 0, GLOBAL);
	  model.GetPosition(pos);

	  fDir[0] = rotAxis[0] - pos[0]; fDir[1] = rotAxis[1] - pos[1]; fDir[2] = -pos[2];

	  //distance = sqrt(fDir[0] * fDir[0] + fDir[1] * fDir[1] + fDir[2] * fDir[2]);
	  model.SetDirection(NULL, temp);
	  model.SetDirection(fDir, NULL);

      oldX = x;
   }
}


/*----------------------------------
  initialize the move of the camera
  C.Wang 0329, 2005
 -----------------------------------*/
void InitMove(int x, int y)
{
   oldXM = x;
   oldYM = y;
   frame = 0;
}

/*------------------
  move the camera
  C.Wang 0329, 2005
 -------------------*/
void MoveCam(float x, float y)
{
	FnViewport vp;
	vp.ID(vID);
	 
	int w, h;
	vp.GetSize(&w, &h);

	float fDir[3], uDir[3], xDir[3], yDir[3];
	float xDirLen, yDirLen, xScalar, yScalar;
	FnObject model;
	model.ID(cID);
	model.GetDirection(fDir, uDir);

	xDir[0] = fDir[1] * uDir[2] - fDir[2] * uDir[1];
	xDir[1] = fDir[2] * uDir[0] - fDir[0] * uDir[2];
	xDir[2] = 0;

	xDirLen = sqrt(xDir[0] * xDir[0] + xDir[1] * xDir[1] + xDir[2] * xDir[2]);

	yDir[0] = -1 * xDir[1];
	yDir[1] = 1 * xDir[0];
	yDir[2] = 0;

	xDir[0] *= x / xDirLen; 
	xDir[1] *= x / xDirLen;
	xDir[2] *= x / xDirLen;
	
	yDirLen = sqrt(yDir[0] * yDir[0] + yDir[1] * yDir[1] + yDir[2] * yDir[2]);
	yDir[0] *= y / yDirLen;
	yDir[1] *= y / yDirLen;
	yDir[2] *= y / yDirLen;

	model.Translate(xDir[0] + yDir[0], xDir[1] + yDir[1], 0, GLOBAL);
	vp.HitPosition(tID, cID, (float)w / 2, (float)h / 2, rotAxis);

   float pos[3];
   model.GetPosition(pos);
   //distance = sqrt((pos[0] - rotAxis[0]) * (pos[0] - rotAxis[0]) + (pos[1] - rotAxis[1]) * (pos[1] - rotAxis[1]) + (pos[2] - rotAxis[2]) * (pos[2] - rotAxis[2]));
}


/*----------------------------------
  initialize the zoom of the camera
  C.Wang 0329, 2005
 -----------------------------------*/
void InitZoom(int x, int y)
{
   oldXMM = x;
   oldYMM = y;
   frame = 0;
}


/*------------------
  zoom the camera
  C.Wang 0329, 2005
 -------------------*/
void ZoomCam(int x, int y)
{
   if (x != oldXMM || y != oldYMM) {
      FnObject model;

      model.ID(cID);
      model.Translate(0.0f, 0.0f, (float)(x - oldXMM)*10.0f, LOCAL);
      oldXMM = x;
      oldYMM = y;
   }
}

// Given new mousePos, move camera if neccessary
void CamMoveCheck(int x, int y) {
    switch(GS){
        case Game:
            FnViewport vp;
            vp.ID(vID);
            int w, h;

            vp.GetSize(&w, &h);
            float halfW = (float)w / 2, halfH = (float)h / 2;

            float xBuff = w * 0.05;
            float yBuff = h * 0.05;
            float newX, newY;
            float shift = 10.f;
            float result[3];

            if (x < xBuff || x > w - xBuff || y < yBuff || y > h - yBuff) {
                int success = vp.HitPosition(tID, cID, x, y, result);
                if (!success)
                    return;
                newX = shift * (float)(x - halfW) / halfW;
                newY = shift * (float)(halfH - y) / halfH;

                if (x == halfW || y == halfH)
                    return;
                if (FyCheckHotKeyStatus(LEFT_MOUSE))
                    return;
                MoveCam(newX, newY);
            }
            break;
    }
}
void test(int x, int y) {

	int w, h;
	FnViewport vp;
	vp.ID(vID);

	screenX = x;
	screenY = y;

	float result[3];
	int success = vp.HitPosition(tID, cID, x, y, result);

	if (success != FAILED_ID) {
		mouseX = result[0];
		mouseY = result[1];
	}
}

void Click(int x, int y){
	switch (GS){
		case MainMenu:
			if (in2D(x, y, 5, 305, 576-110, 576-10)){
				GS = Game;
				UIInit(spsID,MenuID,buttonID1, UIID,headImageID, skillImageID);
				FnMedia mP;
				mP.Object(BGMID);
				mP.Stop();
				
				FyDeleteMediaPlayer(BGMID);
				char buf[260];
				sprintf(buf, "%sBattle01.mp3", UIPath);
				
				BGMID = FyCreateMediaPlayer(buf, 0, 0, 800, 600);
				mP.Object(BGMID);
				mP.Play(LOOP);
				
			}
			break;
        case Game:
            FnObject model;
            model.ID(selectionBoxID);
            int nV, vLen;

            GEOMETRYid geoID = model.GetGeometryID(0);    // we only have one geometric object, so set it as 0
            float *v2d = model.LockVertexBuffer(geoID, &nV, &vLen);
            float sx, sy;

            // calculate the 3D position of the mouse position


            sx = (float)x / w - 0.5f;
            sy = 1.0f - (float)y / h - 0.5f;

            if (v2d != NULL) {
                v2d[0] = sx; v2d[1] = sy;						// vertex 0
                v2d[vLen] = sx; v2d[vLen + 1] = sy;				// vertex 1
                v2d[vLen * 2] = sx; v2d[vLen * 2 + 1] = sy;		// vertex 2
                v2d[vLen * 3] = sx; v2d[vLen * 3 + 1] = sy;		// vertex 3
                v2d[vLen * 4] = sx; v2d[vLen * 4 + 1] = sy;		// vertex 4 (= vertex 0)

                model.UnlockVertexBuffer(geoID);
                model.Show(TRUE);
				updateAllSelectedChar(v2d, vLen, PTSELECTBOX);
				toggleCharShader();
				UpdateUI(selected_chars, headImageID, skillImageID);
                beSelect = TRUE;

            }
            break;
	}
}
void Select(int x, int y){
	
	FnObject model;
	if (beSelect) {
		model.ID(selectionBoxID);
		int nV, vLen;

		GEOMETRYid geoID = model.GetGeometryID(0);    // we only have one geometric object, so set it as 0
		float *v2d = model.LockVertexBuffer(geoID, &nV, &vLen);
		float sx, sy;

		// calculate the 3D position of the mouse position

		sx = (float)x / w - 0.5f;
		sy = 1.0f - (float)y / h - 0.5f;
		if (v2d != NULL) {
			v2d[vLen + 1] = sy;   // vertex 2
			v2d[vLen * 2] = sx; v2d[vLen * 2 + 1] = sy;   // vertex 2
			v2d[vLen * 3] = sx;     // vertex 3
			
			model.UnlockVertexBuffer(geoID);
		}
		// Get all charaters inside the selection box
		updateAllSelectedChar(v2d, vLen, new float[4]{0.0, 0.0, 0.0, 0.0});

		toggleCharShader();

		UpdateUI(selected_chars, headImageID, skillImageID);
		
	}
	
}

void Release(int x, int y){
	if (beSelect) {
		FnObject model(selectionBoxID);
		model.Show(FALSE);
		beSelect = FALSE;
	}
}

void updateAllSelectedChar(float* v2d, int vLen,  const float tol[4]) {
	FnCharacter actor;
	FnViewport vp;
	vp.ID(vID);

	prev_selected_chars = selected_chars;
	selected_chars = std::vector<char_info*>();
	//std::vector<char_info*>().swap(selected_chars);

	float** vertex = new float*[4];
	for (int i = 0; i < 4; i++) {
		vertex[i] = new float[2];
		vertex[i][0] = (v2d[vLen*i] + 0.5) * w ;
		vertex[i][0] += i < 2 ? (-1)*tol[0] : tol[1];
		vertex[i][1] = -(v2d[vLen*i + 1] - 0.5) * h;
		vertex[i][1] += (i==0||i==3) ? (-1)*tol[2] : tol[3];
	}
	char debug[256];
	sprintf(debug, "select size: %d\n", selected_chars.size()); 
	float tPos[3], sPos[2];
	for (auto it = all_char.begin(); it != all_char.end(); ++it) {
		if (it->camp != p1 || it->curPoseID == it->dieID)
			continue;
		it->actor.GetPosition(tPos);
		vp.ComputeScreenPosition(cID, sPos, tPos, PHYSICAL_SCREEN, FALSE);

		bool result = insideRect(vertex, sPos);
		if (result) {
			selected_chars.push_back(&(*it));
			sprintf(debug, "select size: %d\n", selected_chars.size());
		}
	}
}

bool insideRect(float** vertex, float* charPos) {
	float minX = 2000, minY = 2000, maxX = -1000, maxY = -1000;
	for (int i = 0; i < 4; i++) {
		if (vertex[i][0] > maxX)
			maxX = vertex[i][0];
		if (vertex[i][0] < minX)
			minX = vertex[i][0];
		if (vertex[i][1] > maxY)
			maxY = vertex[i][1];
		if (vertex[i][1] < minY)
			minY = vertex[i][1]; 
	}
	if (charPos[0] > minX && charPos[0] < maxX && charPos[1] > minY && charPos[1] < maxY)
		return true;
	return false;
}

void toggleCharShader() {
	for (auto it = selected_chars.begin(); it != selected_chars.end(); it++){
		// Previously rendered selected and currently selected
		if ((prev_selected_chars.end() != std::find(prev_selected_chars.begin(), prev_selected_chars.end(), *it))) {
			continue;
		}
		applyShader((*it)->actorID, "CookTTex3_border");
	}
	for (auto it = prev_selected_chars.begin(); it != prev_selected_chars.end(); it++){
		// Previously rendered selected and currently unselected
		if ((selected_chars.end() == std::find(selected_chars.begin(), selected_chars.end(), *it))) {
			applyShader((*it)->actorID, "CookTTex3");
		}
	}
	
}

void applyShader(CHARACTERid id, char* name) {
	// change the shader of the actor

	int i, j, numM, numS;

	OBJECTid actorModelID;
	FnObject actorModel;
	MATERIALid matID[10];
	FnMaterial mat;
	FnCharacter actor;
	actor.ID(id);

	numS = actor.GetSkinNumber();
	for (i = 0; i < numS; i++) {
		actorModelID = actor.GetSkin(i);
		actorModel.ID(actorModelID);
		numM = actorModel.GetMaterialNumber();
		actorModel.GetMaterials(matID, 10);

		for (j = 0; j < numM; j++) {
			mat.ID(matID[j]);
			mat.AddShaderEffect(name, "CookTColorBumpSpec");
		}
	}
	numS = actor.GetAttachmentNumber();
	for (i = 0; i < numS; i++) {
		actorModelID = actor.GetAttachment(i);
		actorModel.ID(actorModelID);
		numM = actorModel.GetMaterialNumber();
		actorModel.GetMaterials(matID, 10);

		for (j = 0; j < numM; j++) {
			mat.ID(matID[j]);
			mat.AddShaderEffect(name, "CookTColorBumpSpec");
		}
	}
}

#ifndef __MYAI__
#define __MYAI__
#include "Fly.h"
#include "MyCG_util.h"
#include "MyCG_limit.h"
#include "MyCG.h"
#include "skill.h"
#include <vector>
#include <queue>
#include <list>
CHARACTERid findMaxHate(std::map<CHARACTERid, float> hate){
	CHARACTERid maxID=-1; float max=0;
	for (auto it = hate.begin(); it != hate.end(); it++){
		if (it->second > max){
			maxID = it->first;
			max = it->second;
		}
	}
	return max?maxID:-1;
}
float normalAI(void* me, void* all_char=NULL, void* unuse=NULL){
	char_info* _me = (char_info*)me;
	vector<char_info>* _all_char = (vector<char_info>*)all_char;
	// skill 1 is Guard 
	float srcPos[3], dstPos[3], dir[3];
	_me->actor.GetPosition(srcPos);
	_me->actor.GetDirection(dir, NULL);
	for (auto it = _all_char->begin(); it != _all_char->end(); it++){

		if (checkEnemy(_me->camp, it->camp)){	//Check if it can see
			it->actor.GetPosition(dstPos);
			float range = _me->_skillSet[1].sp.range, angle = _me->_skillSet[1].sp.angle;
			if (insideCircle(srcPos, range, dstPos) && insideCone(srcPos, dstPos, angle, range, dir)){
				OutputDebugString("inside circle\n");
				_me->hate[it->actorID] += 0.1;
			}
		}
		if (!it->canDamage)	_me->hate[it->actorID] = 0;
	}

	CHARACTERid nowHate = findMaxHate(_me->hate);
	bool beConfined = _me->act_job==0 && _me->active.front().type == JobType::move && (_me->flag.find(stunned) == _me->flag.end() && _me->flag.find(snared) == _me->flag.end());
	if (nowHate!=-1 && (nowHate != _me->maxHate || beConfined)){	//Hate transform
		char_info* hateChar; 
		auto it = _all_char->begin();
		for (; it != _all_char->end(); it++){
			if (it->actorID == nowHate)
				break;
		}
		_me->maxHate = nowHate;
		_me->active = std::queue<CG_job>();
		_me->active.push(CG_job(new float[3]{-1, -1, -1}, (void*)&(*it), -1.0, JobType::attack, &(_me->_skillSet[0])));
		_me->act_job = 0;
		_me->updatePoseID = _me->runID;
	}
	return 0.0f;
}
#endif
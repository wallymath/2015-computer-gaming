
/*==============================================================
  character movement testing using Fly2

  - Load a scene
  - Generate a terrain object
  - Load a character
  - Control a character to move
  - Change poses

  (C)2012-2015 Chuan-Chang Wang, All Rights Reserved
  Created : 0802, 2012

  Last Updated : 1004, 2015, Kevin C. Wang
 ===============================================================*/
#include "MyCG_util.h"
#include "MyCG.h"
#include "MyCG_stage.h" 
#include "FlyWin32.h"
#include <set>
#include <math.h>       /* acos */


#define PI 3.14159265
#define TRACK_UPPER 900
#define TRACK_LOWER 700
#define H_UPPER 700.0
#define H_LOWER 300.0
#define NORM_ATK_DIS 180.0s

/* Selection Code */
// selection "square"
BOOL4 beSelect = FALSE;
SCENEid s2DID = FAILED_ID;
OBJECTid selectionBoxID = FAILED_ID;
OBJECTid selectCamID = FAILED_ID;

// For testing FX
int playMethod = 2;
OBJECTid dummyID = FAILED_ID;
GAMEFX_SYSTEMid gFXID = FAILED_ID;

VIEWPORTid vID;                 // the major viewport
SCENEid sID;                    // the 3D scene
OBJECTid cID, tID;              // the main camera and the terrain for terrain following
ROOMid terrainRoomID = FAILED_ID;
TEXTid textID = FAILED_ID;
char_info Lyubu, Robber;
std::vector<char_info> all_char;

// some globals
float mouseX, mouseY, mouseZ ;
float screenX, screenY;
float distance;
float tranPos[3];
float rotAxis[3];
std::vector<CHARACTERid> selected_chars;
std::vector<CHARACTERid> prev_selected_chars;

int frame = 0;
float w = 1024.f, h = 768.f;
int oldX, oldY, oldXM, oldYM, oldXMM, oldYMM;
float cpos[3], cfDir[3], cuDir[3];
int outFlag = FALSE;
int BodyID;


void MyCameraMove(float x, float y, bool check=false);
void updateAllSelectedChar(float* v2d, int vLen);
bool insideRect(float** vertex, float* charPos);
void toggleCharShader();
void applyShader(CHARACTERid id, char* name);
inline float dis(float* a, float* b);


// hotkey callbacks
void QuitGame(BYTE, BOOL4);
void Movement(BYTE, BOOL4);
void Combat(BYTE, BOOL4);

// timer callbacks
void GameAI(int);
void RenderIt(int);

// mouse callbacks
void InitPivot(int, int);
void PivotCam(int, int);
void InitMove(int, int);
void MoveCam(float, float);
void Click(int, int);
void Select(int, int);
void Release(int, int);
void InitZoom(int, int);
void ZoomCam(int, int);
void CamMoveCheck(int x, int y, int vID);
void test(int, int);

/*------------------
  the main program
  C.Wang 1010, 2014
 -------------------*/
void FyMain(int argc, char **argv)
{
	// create a new world
	BOOL4 beOK = FyStartFlyWin32("NTU@2014 Homework #01 - Use Fly2", 0, 0, w, h, FALSE);

	// setup the data searching paths
	FySetShaderPath("Data\\NTU6\\Shaders");
	FySetModelPath("Data\\NTU6\\Scenes");
	FySetTexturePath("Data\\NTU6\\Scenes\\Textures");
	FySetScenePath("Data\\NTU6\\Scenes");
	FySetGameFXPath("Data\\NTU6\\FX0");

	// create a viewport
	vID = FyCreateViewport(0, 0, w, h);
	FnViewport vp;
	vp.ID(vID);

	// create a 3D scene
	sID = FyCreateScene(10);
	FnScene scene;
	scene.ID(sID);

	// load the scene
	scene.Load("gameScene02");
	scene.SetAmbientLights(1.0f, 1.0f, 1.0f, 0.6f, 0.6f, 0.6f);

	// load the terrain
	tID = scene.CreateObject(OBJECT);
	FnObject terrain;
	terrain.ID(tID);
	BOOL beOK1 = terrain.Load("terrain");
	terrain.Show(FALSE);
	
	// set terrain environment
	terrainRoomID = scene.CreateRoom(SIMPLE_ROOM, 10);
	FnRoom room;
	room.ID(terrainRoomID);
	room.AddObject(tID);

	// load the character
	FySetModelPath("Data\\NTU6\\Characters");
	FySetTexturePath("Data\\NTU6\\Characters");
	FySetCharacterPath("Data\\NTU6\\Characters");

	// put the character on terrain
#ifdef __CGDEBUG__
	OutputDebugString("In Debug Mode\n");
#endif
	FyTimerReset(0);
	float pos[3] = { 3569.0f, -3208.0f, 0.0f }, fDir[3] = { 1.0f, 1.0f, 0.0f }, uDir[3] = { 0.0f, 0.0f, 1.0f };
	FnCharacter actor;
	load_stage(0, terrainRoomID, sID, all_char);
	
	/*char buf[30];
	sprintf(buf, "Debug: %d\n", all_char.size());
	OutputDebugString(buf);
	*/
	FnCamera camera;
	// translate the camera
	cID = scene.CreateObject(CAMERA);
   
	camera.ID(cID);
	camera.SetNearPlane(5.0f);
	camera.SetFarPlane(100000.0f);
	// set camera initial position and orientation
   
	cpos[0] = 3547.5f; cpos[1] = -4208.0f; cpos[2] = 500.0f;
	//cpos[0] = 4315.783f; cpos[1] = -3199.686f; cpos[2] = 93.046f;
	//Lyubu.actor.GetPosition(pos);
	cfDir[0] = pos[0]-cpos[0]; cfDir[1] = pos[1]-cpos[1]; cfDir[2] = pos[2]-cpos[2];

	cuDir[0] = 0.0f; cuDir[1] = 0.0f; cuDir[2] = 1.0f;
	
	camera.SetPosition(cpos);
	//camera.SetDirection(NULL, cuDir);
	camera.SetDirection(cfDir, NULL);
   
	float mainLightPos[3] = { -4579.0, -714.0, 15530.0 };
	float mainLightFDir[3] = { 0.276, 0.0, -0.961 };
	float mainLightUDir[3] = { 0.961, 0.026, 0.276 };
   
	FnLight lgt;
	lgt.ID(scene.CreateObject(LIGHT));
	lgt.Translate(mainLightPos[0], mainLightPos[1], mainLightPos[2], REPLACE);
	lgt.SetDirection(mainLightFDir, mainLightUDir);
	lgt.SetLightType(PARALLEL_LIGHT);
	lgt.SetColor(1.0f, 1.0f, 1.0f);
	lgt.SetName("MainLight");
	lgt.SetIntensity(0.4f);

	// create a text object for displaying messages on screen
	textID = FyCreateText("Trebuchet MS", 18, FALSE, FALSE);

	// set Hotkeys
	FyDefineHotKey(FY_ESCAPE, QuitGame, FALSE);  // escape for quiting the game
	FyDefineHotKey(FY_UP, Movement, FALSE);      // Up for moving forward
	FyDefineHotKey(FY_RIGHT, Movement, FALSE);   // Right for turning right
	FyDefineHotKey(FY_LEFT, Movement, FALSE);    // Left for turning left
	FyDefineHotKey(FY_DOWN, Movement, FALSE);    // Down for Moving toward camera

   // Combat Keys
   FyDefineHotKey(FY_Q, Combat, FALSE);
   FyDefineHotKey(FY_W, Combat, FALSE);
   FyDefineHotKey(FY_E, Combat, FALSE);
   FyDefineHotKey(FY_R, Combat, FALSE);

	// define some mouse functions
	FyBindMouseMoveFunction(test);
	FyBindMouseFunction(LEFT_MOUSE, Click, Select, Release, NULL);
	FyBindMouseFunction(RIGHT_MOUSE, InitPivot, PivotCam, NULL, NULL);	
	FyBindMouseFunction(MIDDLE_MOUSE, InitZoom, ZoomCam, NULL, NULL);
	//FyBindMouseFunction(RIGHT_MOUSE, InitMove, MoveCam, NULL, NULL);

	// bind timers, frame rate = 30 fps
	FyBindTimer(0, 30.0f, GameAI, TRUE);
	FyBindTimer(1, 30.0f, RenderIt, TRUE);

	// Init rotAxis
	int w, h;
	float result[3];

	vp.GetSize(&w, &h);
	screenX = w / 2; screenY = h / 2;

	int success = vp.HitPosition(tID, cID, screenX, screenY, result);
	if (success != FAILED_ID) {
		mouseX = result[0];
		mouseY = result[1];
		mouseZ = result[2];
	}
	rotAxis[0] = result[0]; rotAxis[1] = result[1]; rotAxis[2] = result[2];

	/* Selection Box */
	// create the "square" object and an orthogal camera to draw the selection region
	s2DID = FyCreateScene();
	scene.ID(s2DID);

	selectionBoxID = scene.CreateObject(MODEL);
	selectCamID = scene.CreateObject(CAMERA);

	FnCamera orthCam(selectCamID);
	orthCam.SetProjectionType(ORTHOGONAL);  // set the camera in orthogonal projection
	orthCam.Translate(0.0f, 0.0f, 100.0f, REPLACE);   // move it higher than the square object
	orthCam.SetAspectRatio(1.0f);     // aspect ratio = 1.0

	// construct the "square" using Lines with green color : 5 vertices in (x, y, z, r, g, b) each
	FnObject selectBox(selectionBoxID);
	float v2d[30], greenColor[3];
	MATERIALid mID = FyCreateMaterial();

	greenColor[0] = 0.0f; greenColor[1] = 1.0f; greenColor[2] = 0.0f;   // green color

	v2d[0] = -0.0f; v2d[1] = 0.0f; v2d[2] = 0.0f;
	v2d[3] = greenColor[0]; v2d[4] = greenColor[1]; v2d[5] = greenColor[2];
	v2d[6] = -0.0f; v2d[7] = -0.0f; v2d[8] = 0.0f;
	v2d[9] = greenColor[0]; v2d[10] = greenColor[1]; v2d[11] = greenColor[2];
	v2d[12] = 0.0f; v2d[13] = -0.0f; v2d[14] = 0.0f;
	v2d[15] = greenColor[0]; v2d[16] = greenColor[1]; v2d[17] = greenColor[2];
	v2d[18] = 0.0f; v2d[19] = 0.0f; v2d[20] = 0.0f;
	v2d[21] = greenColor[0]; v2d[22] = greenColor[1]; v2d[23] = greenColor[2];
	v2d[24] = -0.4f; v2d[25] = 0.4f; v2d[26] = 0.0f;
	v2d[27] = greenColor[0]; v2d[28] = greenColor[1]; v2d[29] = greenColor[2];

	selectBox.Lines(OPEN_POLYLINE, mID, v2d, 5, TRUE, TRUE);

	// hide the selection box as initial
	selectBox.Show(FALSE);
	beSelect = FALSE;

	/* Selection Box */

	// invoke the system
	FyInvokeFly(TRUE);
}


/*-------------------------------------------------------------
  30fps timer callback in fixed frame rate for major game loop
 --------------------------------------------------------------*/
void GameAI(int skip)
{
	std::vector<char_info>::iterator it;
	FnCharacter actor;

	// play game FX
	if (gFXID != FAILED_ID) {
		FnGameFXSystem gxS(gFXID);
		BOOL4 beOK = gxS.Play((float)skip, ONCE);
		if (!beOK) {
			FnScene scene(sID);
			scene.DeleteGameFXSystem(gFXID);
			gFXID = FAILED_ID;
		}
	}
	/*
	char buf[30];
	sprintf(buf, "%f\n", FyTimerCheckTime(0));
	OutputDebugString(buf);
	*/

	/*
	for (it = all_char.begin(); it != all_char.end(); it++){	// enumerate all characters
		
		
		actor.ID(it->actorID);
		float actorPos[3], actorDir[3];
		std::list<CG_event>::iterator itt = it->event_queue.begin();
		while (itt != it->event_queue.end()){//deal w/ buf effect
			bool del_flag = false;
			if (FyTimerCheckTime(0) - (itt->insert) > (itt->delay)){// Start execute
				if (itt->last == -1){	//if no last, exec and remove
					itt->callback((void*)&(itt->atk_attr), (void*)&(*it));
					itt = it->event_queue.erase(itt);
					del_flag = true;
				}
			}
			if (!del_flag)
				itt++;
		}
		//effect
		// Guard -> transform job queue
		CG_job* cur_job;
		if (it->act_job)
			cur_job = &(it->routine[it->job_ptr]);
		else
			cur_job = &(it->active.front());
		
		actor.GetPosition(actorPos);
		float dis_move = dis2D(actorPos, cur_job->dst);
		switch(cur_job->type){	
			case  move:	// move to specific location
				if (dis_move < MIN_DIS){	//Job DONE
					if (cur_job->done != -1){	//First frame after job
						cur_job->done = FyTimerCheckTime(0);
					}
					if (FyTimerCheckTime(0) - cur_job->done > cur_job->last && cur_job->last != -1){
						it->curPoseID = it->runID;
						if (it->act_job)	//character in routine mode, run next routine
							it->job_ptr = (it->job_ptr + 1) % (it->routine.size());
						else{	//active mode
							it->active.pop();
							if (it->active.empty())	//if noting left, back to routine
								it->act_job = 1;
						}
					}
					else{
						it->curPoseID = it->guardID;
					}
					actor.SetCurrentAction(NULL, 0, it->curPoseID);
				}
				else{
					actor.SetDirection(new float[3]{cur_job->dst[0] - actorPos[0], cur_job->dst[1] - actorPos[1], cur_job->dst[2] - actorPos[2]}, NULL);
					if (dis_move < it->stat->_attr._speed.to_float())	//dis_left smaller than char speed, move to target
						actor.MoveForward(dis_move, TRUE, FALSE, 0.0f, TRUE);
					else
						actor.MoveForward(it->stat->_attr._speed.to_float(), TRUE, FALSE, 0.0f, TRUE);
				}
				break;
			case  move_attack:
				break;
			case  attack:
				break;
		}

			// Execute currect job queue
			/*if (!actor.Play(ONCE, (float)skip, FALSE, TRUE)){
				actor.SetCurrentAction(NULL, 0, (*it).curPoseID);
				//damaged = false;
				//Lyubu.combating = false;
			}*/
		//}
		//{
			//actor.Play(LOOP, (float)skip, FALSE, TRUE);
		//}
	//}
	// Check mouse boundary
	/*
   FnCamera camera;
   // play character pose
   actor.ID(Lyubu.actorID);
   float aPos[3], aDir[3], cPos[3], rPos[3], tDir[3] = { 0 };
   actor.GetPosition(aPos, NULL);
   actor.GetDirection(aDir, NULL);
   static bool damaged = false;
   if (Lyubu.combating){
	   if (!damaged && Robber.canDamage){
		   damaged = Lyubu._skillSet[0]((void*)&Lyubu, (void*)&Robber);
		   
		   // Collision detection
		   actor.ID(Robber.actorID);
		   actor.GetPosition(rPos, NULL);
		   float atk_c = asin((aDir[0] * (rPos[1] - aPos[1]) - aDir[1] * (rPos[0] - aPos[0]))/dis(aPos, rPos))*180.0f / PI;
		   float atk_d = acos((aDir[0] * (rPos[0] - aPos[0]) + aDir[1] * (rPos[1] - aPos[1])) / dis(aPos, rPos))*180.0f / PI;
		   if (dis(aPos, rPos) < NORM_ATK_DIS && abs(atk_c)<=20 && atk_d <120){
			   actor.SetCurrentAction(NULL, 0, Robber.dam1ID);
			   if (!Lyubu._skillSet[0]((void*)&Lyubu, (void*)&Robber))
				   actor.SetCurrentAction(NULL, 0, Robber.dieID);
			   damaged = true;
		   }
		   actor.ID(Lyubu.actorID);
		   
	   }
	   if (!actor.Play(ONCE, (float)skip, FALSE, TRUE)){
		   actor.SetCurrentAction(NULL, 0, Lyubu.curPoseID);
		   damaged = false;
		   Lyubu.combating = false;
	   }
   }
   else
		actor.Play(LOOP, (float) skip, FALSE, TRUE);
   */
   //camera.ID(cID);
   //camera.GetDirection(cfDir, NULL);
   // Homework #01 part 1
   // ....

   // Robber face Lyubu
   /*
   actor.ID(Robber.actorID);
   if (actor.GetCurrentAction(NULL) == Robber.dieID)
	   actor.Play(ONCE, (float)skip, FALSE, TRUE);
   else{
	   actor.GetPosition(rPos, NULL);
	   actor.SetDirection(new float[3]{aPos[0] - rPos[0], aPos[1] - rPos[1], aPos[2] - rPos[2]}, NULL);
	   if (actor.GetCurrentAction(NULL) == Robber.dam1ID){
		   if (!actor.Play(ONCE, (float)skip, FALSE, TRUE)){
			   actor.SetCurrentAction(NULL, 0, Robber.curPoseID);
		   }
	   }
	   else
		   actor.Play(LOOP, (float)skip, FALSE, TRUE);
   }*/
}



void MyCameraMove(float x, float y, bool check){
	float tcDir[3], movDir[3], cPos[3], n[3] = {0,0,-1.0f};
	FnCamera camera;
	camera.ID(cID);
	camera.GetDirection(tcDir,NULL);
	cfDir[0] = x;
	cfDir[1] = y;
	cfDir[2] = 0;
	camera.SetDirection(cfDir, NULL);
	camera.MoveForward(10.0, TRUE, FALSE, 0.0f, TRUE);
	FnObject terrain;
	terrain.ID(tID);
	camera.GetPosition(cPos);
	if (check && terrain.HitTest(cPos, n) < 0){	// Camera not on terrain
		camera.MoveForward(-10.0, TRUE, FALSE, 0.0f, TRUE);
		camera.GetPosition(cPos);
		if (cPos[2] < H_UPPER)
			camera.Translate(0, 0, 10.0f, GLOBAL);
		
		
	}
	else if (cPos[2] > H_LOWER)
		camera.Translate(0, 0, -10.0f, GLOBAL);
	camera.SetDirection(tcDir, NULL);
}


/*----------------------
  perform the rendering
  C.Wang 0720, 2006
 -----------------------*/
void RenderIt(int skip)
{
   FnViewport vp;

   // render the whole scene
   vp.ID(vID);
   vp.Render3D(cID, TRUE, TRUE);
   // get camera's data
   FnCamera camera;
   camera.ID(cID);

   float pos[3], fDir[3], uDir[3];
   camera.GetPosition(pos);
   camera.GetDirection(fDir, uDir);

   // show frame rate
   static char string[128];
   if (frame == 0) {
      FyTimerReset(0);
   }

   if (frame/10*10 == frame) {
      float curTime;

      curTime = FyTimerCheckTime(0);
      sprintf(string, "Fps: %6.2f", frame/curTime);
   }

   frame += skip;
   if (frame >= 1000) {
      frame = 0;
   }

   // render the overlapped scene (the square) if necessary
   if (beSelect) {
	   vp.Render3D(selectCamID, FALSE, TRUE);
   }
   
   FnText text;
   text.ID(textID);

   text.Begin(vID);
   text.Write(string, 20, 20, 255, 0, 0);

   char posS[256], fDirS[256], uDirS[256], disS[256], rotAxisS[256], cameraS[256], tsS[256];
   //sprintf(posS, "pos: %8.3f %8.3f %8.3f", pos[0], pos[1], pos[2]);
   sprintf(fDirS, "facing: %8.3f %8.3f %8.3f", fDir[0], fDir[1], fDir[2]);
   sprintf(uDirS, "up: %8.3f %8.3f %8.3f", uDir[0], uDir[1], uDir[2]);
   sprintf(posS, "mouse pos: %f %f", screenX, screenY);
   sprintf(rotAxisS, "rotation Axis: %8.3f, %8.3f, %8.3f", rotAxis[0], rotAxis[1], rotAxis[2]);
   sprintf(disS, "camera to pivot: %8.3f", distance);
   sprintf(cameraS, "camera pos: %8.3f, %8.3f, %8.3f", pos[0], pos[1], pos[2]);
   //sprintf(tsS, "trans pos: %8.3f, %8.3f, %8.3f", tranPos[0], tranPos[1], tranPos[2]);


   text.Write(fDirS, 20, 50, 255, 255, 0);
   text.Write(uDirS, 20, 65, 255, 255, 0);
   text.Write(posS, 20, 35, 255, 255, 0);
   text.Write(disS, 20, 80, 255, 255, 0);
   text.Write(rotAxisS, 20, 95, 255, 255, 0);
   text.Write(cameraS, 20, 110, 255, 255, 0);
   //text.Write(tsS, 20, 125, 255, 255, 0);
   text.End();

   CamMoveCheck(screenX, screenY, vID);

   // swap buffer
   FySwapBuffers();
}


/*------------------
  movement control
 -------------------*/
void Movement(BYTE code, BOOL4 value)
{
   // Homework #01 part 2
   // ....
	FnCharacter actor;
	// play character pose
	/*
	actor.ID(Lyubu.actorID);
	if (value){
		if (code == FY_RIGHT || code == FY_LEFT || code == FY_UP || code == FY_DOWN){
			Lyubu.curPoseID = Lyubu.runID;
			if (!Lyubu.combating)
				actor.SetCurrentAction(NULL, 0, Lyubu.runID);
		}
	}
	else{
		if (!(FyCheckHotKeyStatus(FY_UP) || FyCheckHotKeyStatus(FY_RIGHT) || FyCheckHotKeyStatus(FY_LEFT))){
			Lyubu.curPoseID = Lyubu.idleID;
			if (!Lyubu.combating)
				actor.SetCurrentAction(NULL, 0, Lyubu.idleID);
		}
	}
	*/
}

void Combat(BYTE code, BOOL4 value)
{
	// Homework #03
	// ....
	/*
	FnCharacter actor;
	
	// play character pose
	actor.ID(Lyubu.actorID);
	if (value){
		if (code == FY_Q && !Lyubu.combating){
			actor.SetCurrentAction(NULL, 0, Lyubu.atk1ID);
			Lyubu.combating = true;
		}
	}
	else{
		//actor.SetCurrentAction(NULL, 0, Lyubu.curPoseID);
	}
	*/
}


/*------------------
  quit the demo
  C.Wang 0327, 2005
 -------------------*/
void QuitGame(BYTE code, BOOL4 value)
{
   if (code == FY_ESCAPE) {
      if (value) {
         FyQuitFlyWin32();
      }
   }
}



/*-----------------------------------
  initialize the pivot of the camera
  C.Wang 0329, 2005
 ------------------------------------*/
void InitPivot(int x, int y)
{
   oldX = x;
   oldY = y;
   frame = 0;
   FnViewport vp;
   vp.ID(vID);
   float result[3];
   int success = vp.HitPosition(tID, cID, x, y, result);
   if (success != FAILED_ID) {
	   mouseX = result[0];
	   mouseY = result[1];
   }

   if (FyCheckHotKeyStatus(FY_SHIFT)) {
	   float xyz[3];

	   // hit test the terrain
	   FnViewport vp(vID);
	   GEOMETRYid gID = vp.HitPosition(tID, cID, x, y, xyz);
	   if (gID != FAILED_ID) {
		   FnScene scene(sID);

		   // remove the old one if necessary
		   if (gFXID != NULL) {
			   scene.DeleteGameFXSystem(gFXID);
		   }

		   // create a new game FX system
		   gFXID = scene.CreateGameFXSystem();

		   // case 1 : we create/move a dummy object on the hit position
		   FnGameFXSystem gxS(gFXID);
		   if (playMethod == 1) {
			   if (dummyID == FAILED_ID) {
				   dummyID = scene.CreateObject(MODEL);
			   }

			   FnObject dummy(dummyID);
			   dummy.SetPosition(xyz);

			   // play the FX on it
			   BOOL4 beOK = gxS.Load("SpellHome_01", TRUE);
			   if (beOK) {
				   gxS.SetParentObjectForAll(dummyID);
			   }
		   }
		   else {
			   // play the FX on it
			   BOOL4 beOK = gxS.Load("SpellHome_01", TRUE);
			   if (beOK) {
				   gxS.SetPlayLocation(xyz);
			   }
		   }
	   }
   }
}


/*------------------
  pivot the camera
  C.Wang 0329, 2005
 -------------------*/
void PivotCam(int x, int y)
{
	FnObject model;

   if (x != oldX) {
      model.ID(cID);
	  float angle = (x - oldX) * PI / 180;
	  float fDir[3], pos[3], temp[3] = { 0.f, 0.f, 1.f };

	  model.Translate(-rotAxis[0], -rotAxis[1], 0, GLOBAL);
      model.Rotate(Z_AXIS, (float) (x - oldX), GLOBAL);
	  model.Translate(rotAxis[0], rotAxis[1], 0, GLOBAL);
	  model.GetPosition(pos);

	  fDir[0] = rotAxis[0] - pos[0]; fDir[1] = rotAxis[1] - pos[1]; fDir[2] = -pos[2];

	  distance = sqrt(fDir[0] * fDir[0] + fDir[1] * fDir[1] + fDir[2] * fDir[2]);
	  model.SetDirection(NULL, temp);
	  model.SetDirection(fDir, NULL);

      oldX = x;
   }

   /*
   if (y != oldY) {
      model.ID(cID);
      model.Rotate(X_AXIS, (float) (y - oldY), GLOBAL);
      oldY = y;
   }
   */
}


/*----------------------------------
  initialize the move of the camera
  C.Wang 0329, 2005
 -----------------------------------*/
void InitMove(int x, int y)
{
   oldXM = x;
   oldYM = y;
   frame = 0;
}


/*------------------
  move the camera
  C.Wang 0329, 2005
 -------------------*/
void MoveCam(float x, float y)
{
	FnViewport vp;
	vp.ID(vID);
	 
	int w, h;
	vp.GetSize(&w, &h);

	float fDir[3], uDir[3], xDir[3], yDir[3];
	float xDirLen, yDirLen, xScalar, yScalar;
	FnObject model;
	model.ID(cID);
	model.GetDirection(fDir, uDir);

	xDir[0] = fDir[1] * uDir[2] - fDir[2] * uDir[1];
	xDir[1] = fDir[2] * uDir[0] - fDir[0] * uDir[2];
	xDir[2] = 0;

	xDirLen = sqrt(xDir[0] * xDir[0] + xDir[1] * xDir[1] + xDir[2] * xDir[2]);

	yDir[0] = -1 * xDir[1];
	yDir[1] = 1 * xDir[0];
	yDir[2] = 0;

	xDir[0] *= x / xDirLen; 
	xDir[1] *= x / xDirLen;
	xDir[2] *= x / xDirLen;
	
	yDirLen = sqrt(yDir[0] * yDir[0] + yDir[1] * yDir[1] + yDir[2] * yDir[2]);
	yDir[0] *= y / yDirLen;
	yDir[1] *= y / yDirLen;
	yDir[2] *= y / yDirLen;

	model.Translate(xDir[0] + yDir[0], xDir[1] + yDir[1], 0, GLOBAL);
	vp.HitPosition(tID, cID, (float)w / 2, (float)h / 2, rotAxis);

   float pos[3];
   model.GetPosition(pos);
   distance = sqrt((pos[0] - rotAxis[0]) * (pos[0] - rotAxis[0]) + (pos[1] - rotAxis[1]) * (pos[1] - rotAxis[1]) + (pos[2] - rotAxis[2]) * (pos[2] - rotAxis[2]));
}


/*----------------------------------
  initialize the zoom of the camera
  C.Wang 0329, 2005
 -----------------------------------*/
void InitZoom(int x, int y)
{
   oldXMM = x;
   oldYMM = y;
   frame = 0;
}


/*------------------
  zoom the camera
  C.Wang 0329, 2005
 -------------------*/
void ZoomCam(int x, int y)
{
   if (x != oldXMM || y != oldYMM) {
      FnObject model;

      model.ID(cID);
      model.Translate(0.0f, 0.0f, (float)(x - oldXMM)*10.0f, LOCAL);
      oldXMM = x;
      oldYMM = y;
   }
}

// Given new mousePos, move camera if neccessary
void CamMoveCheck(int x, int y, int vID) {
	FnViewport vp;
	vp.ID(vID);
	int w, h;

	vp.GetSize(&w, &h);
	float halfW = (float)w / 2, halfH = (float)h / 2;

	float xBuff = w * 0.05;
	float yBuff = h * 0.05;
	float newX, newY;
	float shift = 10.f;
	float result[3];

	if (x < xBuff || x > w - xBuff || y < yBuff || y > h - yBuff) {
		int success = vp.HitPosition(tID, cID, x, y, result);
		if (!success)
			return;
		newX = shift * (float)(x - halfW) / halfW;
		newY = shift * (float)(halfH - y) / halfH;

		if (x == halfW || y == halfH)
			return;
		if (FyCheckHotKeyStatus(LEFT_MOUSE))
			return;
		MoveCam(newX, newY);
	}
}
void test(int x, int y) {

	int w, h;
	FnViewport vp;
	vp.ID(vID);

	screenX = x;
	screenY = y;

	float result[3];
	int success = vp.HitPosition(tID, cID, x, y, result);

	if (success != FAILED_ID) {
		mouseX = result[0];
		mouseY = result[1];
	}
}
void Click(int x, int y){
	FnObject model;
	model.ID(selectionBoxID);
	int nV, vLen;

	GEOMETRYid geoID = model.GetGeometryID(0);    // we only have one geometric object, so set it as 0
	float *v2d = model.LockVertexBuffer(geoID, &nV, &vLen);
	float sx, sy;

	// calculate the 3D position of the mouse position


	sx = (float)x / w - 0.5f;
	sy = 1.0f - (float)y / h - 0.5f;

	if (v2d != NULL) {
		v2d[0] = sx; v2d[1] = sy;						// vertex 0
		v2d[vLen] = sx; v2d[vLen + 1] = sy;				// vertex 1
		v2d[vLen * 2] = sx; v2d[vLen * 2 + 1] = sy;		// vertex 2
		v2d[vLen * 3] = sx; v2d[vLen * 3 + 1] = sy;		// vertex 3
		v2d[vLen * 4] = sx; v2d[vLen * 4 + 1] = sy;		// vertex 4 (= vertex 0)

		model.UnlockVertexBuffer(geoID);
		model.Show(TRUE);
		beSelect = TRUE;
	}
}
void Select(int x, int y){
	FnObject model;
	if (beSelect) {
		model.ID(selectionBoxID);
		int nV, vLen;

		GEOMETRYid geoID = model.GetGeometryID(0);    // we only have one geometric object, so set it as 0
		float *v2d = model.LockVertexBuffer(geoID, &nV, &vLen);
		float sx, sy;

		// calculate the 3D position of the mouse position

		sx = (float)x / w - 0.5f;
		sy = 1.0f - (float)y / h - 0.5f;
		if (v2d != NULL) {
			v2d[vLen + 1] = sy;   // vertex 2
			v2d[vLen * 2] = sx; v2d[vLen * 2 + 1] = sy;   // vertex 2
			v2d[vLen * 3] = sx;     // vertex 3
			
			model.UnlockVertexBuffer(geoID);
		}
		// Get all charaters inside the selection box
		updateAllSelectedChar(v2d, vLen);
		toggleCharShader();
	}
}
void Release(int x, int y){
	if (beSelect) {
		FnObject model(selectionBoxID);
		model.Show(FALSE);
		beSelect = FALSE;
	}
}

void updateAllSelectedChar(float* v2d, int vLen) {
	FnCharacter actor;
	FnViewport vp;
	vp.ID(vID);

	prev_selected_chars = selected_chars;
	std::vector<CHARACTERid>().swap(selected_chars);

	float** vertex = new float*[4];
	for (int i = 0; i < 4; i++) {
		vertex[i] = new float[2];
		vertex[i][0] = (v2d[vLen*i] + 0.5) * w;
		vertex[i][1] = -(v2d[vLen*i + 1] - 0.5) * h;
	}

	float tPos[3], sPos[2];
	for (std::vector<char_info>::iterator it = all_char.begin(); it != all_char.end(); ++it) {
		actor.ID(it->actorID);
		actor.GetPosition(tPos);
		vp.ComputeScreenPosition(cID, sPos, tPos, PHYSICAL_SCREEN, FALSE);

		bool result = insideRect(vertex, sPos);
		if (result) {
			selected_chars.push_back(it->actorID);
		}
	}
}

bool insideRect(float** vertex, float* charPos) {
	float minX = 2000, minY = 2000, maxX = -1000, maxY = -1000;
	for (int i = 0; i < 4; i++) {
		if (vertex[i][0] > maxX)
			maxX = vertex[i][0];
		if (vertex[i][0] < minX)
			minX = vertex[i][0];
		if (vertex[i][1] > maxY)
			maxY = vertex[i][1];
		if (vertex[i][1] < minY)
			minY = vertex[i][1]; 
	}
	if (charPos[0] > minX && charPos[0] < maxX && charPos[1] > minY && charPos[1] < maxY)
		return true;
	return false;
}

void toggleCharShader() {
	std::vector<CHARACTERid>::iterator id;
	for (id = selected_chars.begin(); id != selected_chars.end(); id++){
		// Previously rendered selected and currently selected
		if ((prev_selected_chars.end() != std::find(prev_selected_chars.begin(), prev_selected_chars.end(), *id))) {
			continue;
		}
		applyShader(*id, "CookTTex3_border");
	}
	for (id = prev_selected_chars.begin(); id != prev_selected_chars.end(); id++){
		// Previously rendered selected and currently unselected
		if ((selected_chars.end() == std::find(selected_chars.begin(), selected_chars.end(), *id))) {
			applyShader(*id, "CookTTex3");
		}
	}
}

void applyShader(CHARACTERid id, char* name) {
	// change the shader of the actor

	int i, j, numM, numS;

	OBJECTid actorModelID;
	FnObject actorModel;
	MATERIALid matID[10];
	FnMaterial mat;
	FnCharacter actor;
	actor.ID(id);

	numS = actor.GetSkinNumber();
	for (i = 0; i < numS; i++) {
		actorModelID = actor.GetSkin(i);
		actorModel.ID(actorModelID);
		numM = actorModel.GetMaterialNumber();
		actorModel.GetMaterials(matID, 10);

		for (j = 0; j < numM; j++) {
			mat.ID(matID[j]);
			mat.AddShaderEffect(name, "CookTColorBumpSpec");
		}
	}
	numS = actor.GetAttachmentNumber();
	for (i = 0; i < numS; i++) {
		actorModelID = actor.GetAttachment(i);
		actorModel.ID(actorModelID);
		numM = actorModel.GetMaterialNumber();
		actorModel.GetMaterials(matID, 10);

		for (j = 0; j < numM; j++) {
			mat.ID(matID[j]);
			mat.AddShaderEffect(name, "CookTColorBumpSpec");
		}
	}
}

#ifndef __MYCGUTIL__
#define __MYCGUTIL__
#include <math.h>
#include <cstdio>
#include "FlyWin32.h"
typedef float(*fptr)(void*, void*, void*);
struct rf{
	float _max;
	float _min;
	float _cur;
	rf(float max, float min, float cur) : _max(max), _min(min), _cur(cur){}
	rf& operator =(float cur)
	{
		_cur = cur;
		if (_cur > _max) _cur = _max;
		if (_cur < _min) _cur = _min;
		return *this;
	}
	rf& operator +=(float c)
	{
		_cur += c;
		if (_cur > _max) _cur = _max;
		if (_cur < _min) _cur = _min;
		return *this;
	}
	rf& operator -=(float c)
	{
		_cur -= c;
		if (_cur > _max) _cur = _max;
		if (_cur < _min) _cur = _min;
		return *this;
	}
	rf& operator *(float c)
	{
		_cur *= c;
		if (_cur > _max) _cur = _max;
		if (_cur < _min) _cur = _min;
		return *this;
	}
	bool operator >(float c){ return _cur > c; }
	bool operator <(float c){ return _cur < c; }
	bool operator ==(float c){ return _cur == c; }
	float ratio(){ return (_cur - _min) / (_max - _min); }
	float to_float(){ return _cur; }
};
enum CampType		{enemy, p1, p2};
enum JobType		{move, move_attack, attack };
enum GameStatus		{MainMenu, Game, Save, Load};
enum SkillType		{heal, damage};
enum DirectionType	{cone, line, circle, target};
enum AIType			{normal};
enum SPEffect		{none, stunned, snared, silent, knock_away, debuf};
struct ATKparams{
	float _minDis, _maxDis, _width, _degree;
	ATKparams(float minDis = 0.0f, float maxDis = 1000.0f, float width = 30.0f, float degree = 0.0f) : _minDis(minDis), _maxDis(maxDis), _width(width), _degree(degree){}
};

inline bool in2D(float x, float y, float l, float r, float b, float t){
	return (x<r&&x>l&&y<t&&y>b);
}
inline float dis(float* a, float* b){
	return sqrt(pow(a[0] - b[0], 2) + pow(a[1] - b[1], 2) + pow(a[2] - b[2], 2));
}
inline float dis2D(float* a, float* b){
	return sqrt(pow(a[0] - b[0], 2) + pow(a[1] - b[1], 2));
}
inline void printPos(float* a){
	char msg[50];
	sprintf(msg, "%.2f, %.2f, %.2f\n", a[0],a[1],a[2]);
	OutputDebugString(msg);
}
inline void normalize(float in[], int num) {
	float sum = 0.f;
	for (int i = 0; i < num; i++) {
		sum += pow(in[i], 2);
	}
	sum = sqrt(sum);
	for (int i = 0; i < num; i++) {
		in[i] = in[i] / sum;
	}
}
#endif